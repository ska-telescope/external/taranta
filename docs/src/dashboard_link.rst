Dashboard Link Widget
*********************

The Dashboard Link widget provides a convenient way to link to other dashboards. 

It can be customized to open the dashboard in the same tab or a new tab. 

The widget also allows for custom styling to fit seamlessly into any dashboard design.

This widget uses the dashboard name to create a direct link to it, if for some reason that 
dashboard gets to be renamed the widget will try to link it by the ID.

Widget setting 
==============

Widget configuration:

\ |IMG1|\

Once the widget is added, you can select the default dashboard that the link should point to.

\ |IMG2|\

The widget also provides advanced styling options to customize the appearance of the dropdown and button.

\ |IMG3|\

It is possible to customize the widget setting in the backend, as the following table.

Widget options 
==============

+---------------------+----------------------------------------------------+
|Input value          |Description                                         |
+=====================+====================================================+
|DefaultDashboard     |Select the default dashboard to link to             |
+---------------------+----------------------------------------------------+
|HideDropdown         |Boolean to decide if dropdown is visible or not     |
+---------------------+----------------------------------------------------+
|NewTab               |Boolean to decide if the link opens in a new tab    |
+---------------------+----------------------------------------------------+
|ButtonText           |Text to display on the button                       |
+---------------------+----------------------------------------------------+
|DropDownCss          |Custom CSS for the dropdown                         |
+---------------------+----------------------------------------------------+
|ButtonCss            |Custom CSS for the button                           |
+---------------------+----------------------------------------------------+
|CustomCss            |Custom CSS for the entire widget                    |
+---------------------+----------------------------------------------------+

Applying CSS to widget
======================

The look and feel of widgets can be customised using the ``Custom CSS`` section. CSS syntax is the same as that used for HTML files.

\ |IMG4|\

.. bottom of content

.. |IMG1| image:: _static/img/dashboard_link_widget.png
   :width: 289 px

.. |IMG2| image:: _static/img/dashboard_link_select.png
   :width: 339 px

.. |IMG3| image:: _static/img/dashboard_link_custom_css.png
   :width: 598 px

.. |IMG4| image:: _static/img/dashboard_link_widget_css.png
   :width: 574 px

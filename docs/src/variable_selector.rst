Variable Selector Widget
************************

The widget enables the user to use dashboard variable and change device associated with the dashboard variable in run mode

Widget setting 
===============

The user has the ability to select:

* A free label for the widget (string), shown before variable (leave blank to hide)
* Variable dropdown to select dashboard variable. User would be able to change device name for this variable in run mode


\ |IMG1|\ 


When a user changes the dropdown value, newly selected device value is assigned to dashboard variable which was configured in inspector panel.


\ |IMG2|\ 


*Notice: Once user changes variable's device, this will result in change of device value for all the widgets subscribed to given variable.*


\ |IMG3|\ 


.. bottom of content

.. |IMG1| image:: _static/img/variable_selector_settings.png
   :height: 190 px
   :width: 250 px

.. |IMG2| image:: _static/img/variable_selector_run_mode.png
   :height: 200 px
   :width: 290 px

.. |IMG3| image:: _static/img/variable_selector_device_change.png
   :height: 450 px
   :width: 600 px

Attribute Scatter Widget
************************

A widget that plots value of two attributes on X & Y axes.

Widget setting 
==============
The widget has two attributes plotted on X & Y axes, first attribute on X-axis other one on Y-axis.
The value of two attributes is plotted using ``+`` marker can be hovered to view the actual value.
The value of X & Y axes increases depending on the attributes value. At a given time widget shows all the values of attributes from begining.

\ |IMG1|\ 


Widget design
==============
Taranta is using the Plotly framwork to render the plots. The range of the X & Y axes self-adjusts to fit all data points for every plot being displayed. By hovering over the widget in run mode, additional options are made available from a toolbar, such as zooming and panning. These come from the plotly framwork itself, but the functionality of two of them have been overwritten to better suit Taranta. By clicking ``autoscale``, the time-window mode is disabled, and the y-axis instead covers all data points it has registered so far. To go back to the windowed mode, click on ``Reset axis``.
This widget gives clear picture about range of value for two attributes.


\ |IMG2|\ 

*The same plot in run mode, showing all the data points received from begining for both of the attributes.*

.. bottom of content

.. |IMG1| image:: _static/img/attribute_scatter_input.png

.. |IMG2| image:: _static/img/attribute_scatter_design.png
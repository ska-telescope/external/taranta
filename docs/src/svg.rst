Working with SVG
================

.. toctree::
    :maxdepth: 1

    svg_inkscape
    svg_link_device_to_svg
    svg_widget
    svg_custom_css
    svg_format
    svg_rules
    svg_zoom


Example SVG file 
----------------

.. toctree::
    :maxdepth: 1

    sampleSVG

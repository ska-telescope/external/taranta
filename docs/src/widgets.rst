Available widgets
*****************

A widget is a component of a dashboard that lets you interact with Tango devices. A widget
can let you see how an attribute changes over time, or its current value; a widget can let you send a command to a device or change an attribute or its state.

What follows is the list of existing widgets.


Dealing with labels 
===================

.. toctree::
    :maxdepth: 1

    label
    label_display

Dealing with attributes
=======================

.. toctree::
    :maxdepth: 1

    device_status
    display
    boolean_display
    led
    tabular_view
    attribute_writer
    attribute_writer_dropdown
    attribute_plot
    attribute_scatter
    timeline
    attribute_logger
    attribute_heatmap
    attribute_dial


Dealing with commands 
=====================

.. toctree::
    :maxdepth: 1

    command_writer
    command
    command_file
    command_switch

Dealing with spectrum
=====================

.. toctree::
    :maxdepth: 1

    spectrum
    spectrum_2d
    spectrum_table

Dealing with grouping of widgets
================================

.. toctree::
    :maxdepth: 1

    box

Dealing with dashboard 
======================

.. toctree::
    :maxdepth: 1

    variable_selector
    embed_page
    dashboard_link

Dealing with logs 
=================

.. toctree::
    :maxdepth: 1

    elasticsearch_viewer

Dealing with SVG 
================

.. toctree::
    :maxdepth: 1

    svg_inkscape
    svg_link_device_to_svg
    svg_widget

Dealing with images 
===================

.. toctree::
    :maxdepth: 1

    image_display
    image_table

Dealing with Sardana 
====================

.. toctree::
    :maxdepth: 1

    sardana_motor
    macro_button

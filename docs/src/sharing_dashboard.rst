Sharing dashboard
*****************

Taranta allows you to create custom dashboards, save them, export them, import them, clone them, and even share them with a group of users.

Dashboards are saved in a MongoDB repository accessed by Taranta using a REST API based on Express. Further project details are available at the following link `Taranta Dashboard <https://taranta-dashboard.readthedocs.io/en/latest/>`_.

Sharing Dashboards
==================

Once a user creates a dashboard, it is initially visible only to that user and not to others. In the 'Dashboard' section in the top menu, there is a 'Share' function. This function enables users to share the dashboard with a group of users. In addition to selecting the group, the user can also choose whether to allow editing by the users with whom it is shared. There are different groups based on Taranta's configuration.

Once a dashboard is shared, it becomes visible to other users with the appropriate permissions. It appears in the 'Dashboard' tab, under the 'Shared Dashboard' section, just below the user's library of dashboards. At that point, users can edit it (if granted permission), run it, or clone it into their own library for free use.

Environment
===========

Depending on the type of installation performed, whether using NPM, Docker, or Kubernetes, you can access Taranta through different links specifying different environments.

The basic installation using NPM or Docker will provide a link like this:

`https://<domain>/<tango_db>/dashboard`

Where <domain> represents the domain hosting Taranta, and <tango_db> is the default TANGO Database.

With this type of installation, you usually access all stored dashboards connected to the Taranta installation. Therefore, in the case of sharing, it will be possible to share dashboards only with users who have access to that installation.

If Kubernetes is used, Taranta can respond to a different type of URL, identified as follows:

`https://<domain>/<namespace>/<tango_db>/dashboard`

In this case, only <namespace> is added, identifying a Kubernetes namespace. By default, Taranta allows the sharing of dashboards within its namespace. For example, if a user creates and shares a dashboard with "Group 1" in this environment:

`https://mytaranta.com/taranta_namespace/my_db/dashboard`

All users who have access to the taranta_namespace namespace and belong to "Group 1" can view the dashboard or edit it if they have the necessary permissions.

If desired, the user can also share the dashboard with another namespace or another domain, such as:

`https://mytaranta.com/other_namespace/my_db/dashboard`

In this case, the domain remains the same, but the created dashboard is also available in the other_namespace namespace.

Alternatively,

`https://othertaranta.com/taranta_namespace/my_db/dashboard`

In this example, the dashboard is shared in the same taranta_namespace namespace but belongs to the othertaranta domain.

It is also possible to have more open rules, where you can share the dashboard across all namespaces or domains simultaneously using the * (wildcard) character.

For example:

`https://mytaranta.com/*/my_db/dashboard`

Allows sharing the dashboard in all namespaces of the mytaranta domain.

Or even:
`*/*`

Allows sharing in all namespaces and all domains.

To understand how to share the dashboard in various namespaces following different rules, please refer to the related documentation `dashboard_env.html``


Admin user
==========

Every user can edit their dashboard, including changing the conditional rules between user groups and namespaces.

However, there is a special user, the ADMIN user, who has access to all dashboards 
within the accessed namespace and is free to modify them according to their needs. 

Attribute Name or Label display
*******************************

Taranta as a set of options that allow the user to define either to display attribute name on a widget, it's associated label or neither


Example AttributeDisplay widget: 
================================

It is possible to define what will be displayed on a specify widget as we can see from the following image:

\ |IMG1|\ 


In this case, we can display attribute Label (RANDOM) or Name (random) or None () among other options

\ |IMG2|\

\ |IMG3|\

\ |IMG4|\

Taranta overview and labels: 
============================

In Taranta overview the labels are displayed by default as can be seen on this image:

\ |IMG5|\

.. bottom of content

.. |IMG1| image:: _static/img/attributeDisplay.png
   :height: 566 px
   :width: 283 px

.. |IMG2| image:: _static/img/label.png
   :height: 48 px
   :width: 626 px

.. |IMG3| image:: _static/img/name.png
   :height: 48 px
   :width: 626 px

.. |IMG4| image:: _static/img/none.png
   :height: 48 px
   :width: 626 px

.. |IMG5| image:: _static/img/overview.png
   :height: 372 px
   :width: 753 px

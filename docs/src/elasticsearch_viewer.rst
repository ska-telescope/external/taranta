Elasticsearch Log viewer Widget
*******************************

***
Update on 06/04/2023 this widget now only works with STFC and uses \ 
taranta-dashboards backend to communicate with it, if the URL is \ 
not accepted a warning message will appear
***

A widget that shows the log coming from an Elasticsearch server. 

Widget settings 
===============

Elastic widget all available filters:
-------------------------------------

+--------------------+--------------------------------------------------------------+
|Filter              |Description                                                   |
+====================+==============================================================+
|Show log as text    |Switches message fields display between table or text         |
+--------------------+--------------------------------------------------------------+
|Search input        |Allows text introduction to filter message field              |
+--------------------+--------------------------------------------------------------+
|From date           |Date/Time introduction to filter FROM message timestamp field |
+--------------------+--------------------------------------------------------------+
|To date             |Date/Time introduction to filter TO message timestamp field   |
+--------------------+--------------------------------------------------------------+
|Multiple Device     |Allows multiple device dropdown selection                     |
+--------------------+--------------------------------------------------------------+
|Log level           |Allows log level dropdown selection                           |
+--------------------+--------------------------------------------------------------+
|Refresh             |Sets the refresh time to query elasticsearch                  |
+--------------------+--------------------------------------------------------------+
|Show overflow scroll|Forces scroll on the widget box                               |
+--------------------+--------------------------------------------------------------+
|Custom Css          |It permits to customize the CSS of the widget                 |
+--------------------+--------------------------------------------------------------+

|IMG1|

Elastic widget device selection filter:
---------------------------------------

|IMG3|

Elastic widget log level selection filter:
------------------------------------------

|IMG4|

Elastic widget refresh time filter:
-----------------------------------

|IMG5|

Elastic date/time selection filter:
-----------------------------------

|IMG6|

Elastic widget type (table or text) of display selection:
---------------------------------------------------------

|IMG7|


Widget RunCanvas
================

RunCanvas live filters available:
---------------------------------

|IMG2|

Elastic widget fields as columns:
---------------------------------

|IMG8|

Elastic widget fields as text:
------------------------------

|IMG9|

.. bottom of content

.. |IMG1| image:: _static/img/ElasticInspector.png
   :height: 431 px
   :width: 258 px

.. |IMG2| image:: _static/img/ElasticLiveFilters.png
   :height: 208 px
   :width: 506 px

.. |IMG3| image:: _static/img/ElasticDevice.png
   :height: 377 px
   :width: 263 px

.. |IMG4| image:: _static/img/ElasticLogLevel.png
   :height: 238 px
   :width: 267 px

.. |IMG5| image:: _static/img/ElasticRefresh.png
   :height: 189 px
   :width: 281 px

.. |IMG6| image:: _static/img/ElasticDates.png
   :height: 342 px
   :width: 387 px

.. |IMG7| image:: _static/img/ElasticType.png
   :height: 73 px
   :width: 280 px

.. |IMG8| image:: _static/img/ElasticTable.png
   :height: 287 px
   :width: 720 px

.. |IMG9| image:: _static/img/ElasticText.png
   :height: 298 px
   :width: 715 px

Attribute Display Widget
************************

The widget displays the value of a certain attribute, it can display the raw value, or in case of DevEnum types, it can also display the correspondent label
The widget displays the value of a certain attribute; it can display the raw value, or in the case of DevEnum types, it can also display the correspondent label.

Widget setting 
==============

Widget configuration: 

\ |IMG1|\ 

Once the device is selected, the widget will display it's attributes, if they are of the type DevEnum a symbol (*[...]*) will be display in front of the attribute name. 

\ |IMG2|\ 

If the value from attribute is a string with JSON the widget displays it in the form of selectable keys, as follows:

\ |IMG3|\ 

It is possible to customize the widget setting in the backend, as the following table. 

Widget options 
==============

+---------------------+----------------------------------------------------+
|Input value          |Description                                         |
+=====================+====================================================+
|Precision            |Integer for defining significative numbers of value |
+---------------------+----------------------------------------------------+
|Format               |It allows to format a number.                       |
|                     |Please refer to the section below for more details. |
+---------------------+----------------------------------------------------+
|Show Unit            |Boolean for displaying unit, if available           |
+---------------------+----------------------------------------------------+
|Device Name          |Boolean for displaying device name                  |
+---------------------+----------------------------------------------------+
|Attribute Display    |Label, Name or None                                 |
+---------------------+----------------------------------------------------+
|Align text on center |Boolean to align text on center or not              |
+---------------------+----------------------------------------------------+
|Align value on right |Boolean to align value on right or not              |
+---------------------+----------------------------------------------------+
|Scientific Notation  |Boolean for displaying scientific notation          |
+---------------------+----------------------------------------------------+
|Show Enum Labels     |Boolean for displaying enum labels                  |
+---------------------+----------------------------------------------------+
|Show Attr Quality    |Boolean for displaying the attribute value quality  |
+---------------------+----------------------------------------------------+
|Text color           |Color to be apllied to text                         |
+---------------------+----------------------------------------------------+
|Background color     |Color to be apllied to background                   |
+---------------------+----------------------------------------------------+
|Text size in units   |1 corresponds to 1em text size                      |
+---------------------+----------------------------------------------------+
|Font type            |Default or Monospaced                               |
+---------------------+----------------------------------------------------+
|Custom CSS           |Custom CSS to be apllied to widget                  |
+---------------------+----------------------------------------------------+

JSON
====

If the attribute you're viewing is a JSON, Taranta automatically applies proper formatting. By default, the JSON is collapsed and the user can navigate the tree, but you can expand it entirely by unchecking 'JSON collapsed' in the Inspector panel.

It is also possible to view specific parts of the JSON using XPath. The XPath should be specified in the 'Format' field of the Inspector panel. Let's consider the following JSON as an example

.. code-block:: JSON

   {
      "routes":[
      0:{
      "src":{
      "channel":96
      }
      "dst":{
      "port":3
      }
      }
      1:{
      "src":{
      "channel":235
      }
      "dst":{
      "port":16
      }
      }
      ]
   }

Using the following xPath it is possibile to see filter the output. 

+--------------------------+--------------------------------------------------------+
|Format                    |Result                                                  |
+==========================+========================================================+
|routes[0].src.channel     |96                                                      |
+--------------------------+--------------------------------------------------------+
|routes[0]                 |{"src":{"channel":96},"dst":{"port":12}}                |
+--------------------------+--------------------------------------------------------+



It is possible to manipulate data. For example it is possible to concat strings as the following example. 

+-------------------------------------------------------+--------------------------------------------------------+
|Format                                                 |Result                                                  |
+=======================================================+========================================================+
|routes[0].src.channel & ' ' &routes[1].src.channel     |96 235                                                  |
+-------------------------------------------------------+--------------------------------------------------------+
|routes[0].src.channel + routes[1].src.channel          |{"src":{"channel":96},"dst":{"port":12}}                |
+-------------------------------------------------------+--------------------------------------------------------+


More detail are avaible in the `JSONAta documentation <https://docs.jsonata.org/simple>`_  

In the event that the XPath expression yields no results, you will see the message 'No result evaluating JSON expression,' whereas if the expression is syntactically incorrect, you will encounter the message 'Error evaluating JSON expression.'

Formatting a number
===================

If a user wants to apply specific formatting to numbers, specifying, for example, the notation type to use and how many decimal places to display, they can use the 'Format' field. The 'Format' field is only valid if the data is of numeric type; otherwise, it will be ignored.

By default, it has no value, and therefore, the number will be displayed as it comes from the data source. If it is populated, it will override the configurations defined in 'Precision' and 'Scientific Notation.'

There are two possible format styles. One is based on "placeholders," using a number or character to represent significant digits and special characters to represent decimals, percentages, and so on. The other style is based on C-like syntax.

The following tables demonstrate how to use the 'Format' field to format a number

**C-like style**

To use the c-like style format, start the string using the special char '%' and add a char following the list: 

- b — yields an integer as a binary number
- c — yields an integer as the character with that ASCII value
- d or i — yields an integer as a signed decimal number
- e — yields a float using scientific notation
- u — yields an integer as an unsigned decimal number
- f or g — yields a float as is;
- o — yields an integer as an octal number
- t — yields true or false
- x — yields an integer as a hexadecimal number (lower-case)
- X — yields an integer as a hexadecimal number (upper-case)
- s - yields a number as a sexagesimal

If you use sexagesimal conversion, you can specify the precision in the Inspector precision field. 

Examples

+--------------------------+------------------+-------------------------------------+
| Number                   | Format           | String                              |
+==========================+==================+=====================================+
| -187                     | %b               | 11011001                            |
+--------------------------+------------------+-------------------------------------+
| 255                      | %c               | !                                   |
+--------------------------+------------------+-------------------------------------+
| -242.05                  | %d               | -242                                |
+--------------------------+------------------+-------------------------------------+
| -116.22                  | %e               | -1.1622268769557515e+2              |
+--------------------------+------------------+-------------------------------------+
| -87.56                   | %u               | 87                                  |
+--------------------------+------------------+-------------------------------------+
| 201.73                   | %d               | 201.7306321011084                   |
+--------------------------+------------------+-------------------------------------+
| 255                      | %f or %g         | !                                   |
+--------------------------+------------------+-------------------------------------+
| 170                      | %o               | 253                                 |
+--------------------------+------------------+-------------------------------------+
| 250                      | %x               | fa                                  |
+--------------------------+------------------+-------------------------------------+
| 250                      | %X               | FA                                  |
+--------------------------+------------------+-------------------------------------+
| 10.4                     | %s               | 10:54:0.0.00                        |
+--------------------------+------------------+-------------------------------------+

**Manipulattion**

There are basic functions introduced in case you need to manipulate with final data (add offset, change to kilo- or mili-).

+------------------+------------------+--------------------+
| Number           | Format           | String             |
+==================+==================+====================+
| 1000             | .add(100)        | 1100               |
+------------------+------------------+--------------------+
| 1000             |.subtract(100)    | 900                |
+------------------+------------------+--------------------+
| 1000             |.multiply(100)    | 1000000            |
+------------------+------------------+--------------------+
| 1000             |.divide(100)      | 10                 |
+------------------+------------------+--------------------+

**PLACEHOLDER STYLE**

**Number**

+--------------------------+------------------+-------------------------------------+
| Number                   | Format           | String                              |
+==========================+==================+=====================================+
| 10000                    | '0,0.0000'       | 10,000.0000                         |
+--------------------------+------------------+-------------------------------------+
| 10000.23                 | '0,0'            | 10,000                              |
+--------------------------+------------------+-------------------------------------+
| 10000.23                 | '+0,0'           | +10,000                             |
+--------------------------+------------------+-------------------------------------+
| -10000                   | '0,0.0'          | -10,000.0                           |
+--------------------------+------------------+-------------------------------------+
| 10000.1234               | '0.000'          | 10000.123                           |
+--------------------------+------------------+-------------------------------------+
| 100.1234                 | '00000'          | 00100                               |
+--------------------------+------------------+-------------------------------------+
| 1000.1234                | '000000,0'       | 001,000                             |
+--------------------------+------------------+-------------------------------------+
| 10                       | '000.00'         | 010.00                              |
+--------------------------+------------------+-------------------------------------+
| 10000.1234               | '0[.]00000'      | 10000.12340                         |
+--------------------------+------------------+-------------------------------------+
| -10000                   | '(0,0.0000)'     | (10,000.0000)                       |
+--------------------------+------------------+-------------------------------------+
| -0.23                    | '.00'            | -.23                                |
+--------------------------+------------------+-------------------------------------+
| -0.23                    | '(.00)'          | (.23)                               |
+--------------------------+------------------+-------------------------------------+
| 0.23                     | '0.00000'        | 0.23000                             |
+--------------------------+------------------+-------------------------------------+
| 0.23                     | '0.0[0000]'      | 0.23                                |
+--------------------------+------------------+-------------------------------------+
| 1230974                  | '0.0a'           | 1.2m                                |
+--------------------------+------------------+-------------------------------------+
| 1460                     | '0 a'            | 1 k                                 |
+--------------------------+------------------+-------------------------------------+
| -104000                  | '0a'             | -104k                               |
+--------------------------+------------------+-------------------------------------+
| 1                        | '0o'             | 1st                                 |
+--------------------------+------------------+-------------------------------------+
| 100                      | '0o'             | 100th                               |
+--------------------------+------------------+-------------------------------------+

**Currency**

+--------------------------+---------------+-------------------+
| Number                   | Format        | String            |
+==========================+===============+===================+
| 1000.234                 | '$0,0.00'     | $1,000.23         |
+--------------------------+---------------+-------------------+
| 1000.2                   | '0,0[.]00 $'  | 1,000.20 $        |
+--------------------------+---------------+-------------------+
| 1001                     | '$ 0,0[.]00'  | $ 1,001           |
+--------------------------+---------------+-------------------+
| -1000.234                | '($0,0)'      | ($1,000)          |
+--------------------------+---------------+-------------------+
| -1000.234                | '$0.00'       | -$1000.23         |
+--------------------------+---------------+-------------------+
| 1230974                  | '($ 0.00 a)'  | $ 1.23 m          |
+--------------------------+---------------+-------------------+


**Bytes**

+------------------------+--------------+---------------+
| Number                 | Format       | String        |
+========================+==============+===============+
| 100                    | '0b'         | 100B          |
+------------------------+--------------+---------------+
| 1024                   | '0b'         | 1KB           |
+------------------------+--------------+---------------+
| 2048                   | '0 ib'       | 2 KiB         |
+------------------------+--------------+---------------+
| 3072                   | '0.0 b'      | 3.1 KB        |
+------------------------+--------------+---------------+
| 7884486213             | '0.00b'      | 7.88GB        |
+------------------------+--------------+---------------+
| 3467479682787          | '0.000 ib'   | 3.154 TiB     |
+------------------------+--------------+---------------+


**Percentages**

+------------------------+-------------+-------------+
| Number                 | Format      | String      |
+========================+=============+=============+
| 1                      | '0%'        | 100%        |
+------------------------+-------------+-------------+
| 0.974878234            | '0.000%'    | 97.488%     |
+------------------------+-------------+-------------+
| -0.43                  | '0 %'       | -43 %       |
+------------------------+-------------+-------------+
| 0.43                   | '(0.000 %)' | 43.000 %    |
+------------------------+-------------+-------------+



**Time**

+------------------------+-------------+-----------+
| Number                 | Format      | String    |
+========================+=============+===========+
| 25                     | '00:00:00'  | 0:00:25   |
+------------------------+-------------+-----------+
| 238                    | '00:00:00'  | 0:03:58   |
+------------------------+-------------+-----------+
| 63846                  | '00:00:00'  | 17:44:06  |
+------------------------+-------------+-----------+


**Exponential**

+------------------------+------------+--------------+
| Number                 | Format     | String       |
+========================+============+==============+
| 1123456789             | '0,0e+0'   | 1e+9         |
+------------------------+------------+--------------+
| 12398734.202           | '0.00e+0'  | 1.24e+7      |
+------------------------+------------+--------------+
| 0.000123987            | '0.000e+0' | 1.240e-4     |
+------------------------+------------+--------------+




Applying CSS to widget
======================

The look and feel of widgets can be customised using the ``Custom CSS`` section. CSS syntax is the same as that used for HTML files.

\ |IMG4|\

Above CSS rules written in the ``Custom Css`` section are applied to the widget as below:

\ |IMG5|\

.. bottom of content

.. |IMG1| image:: _static/img/display_widget_1.png
   :width: 290 px

.. |IMG2| image:: _static/img/display_widget_2.png
   :height: 45 px
   :width: 311 px

.. |IMG3| image:: _static/img/jsonDisplay.png
   :height: 204 px
   :width: 222 px

.. |IMG4| image:: _static/img/attribute_display_css_setting.png
   :height: 616 px
   :width: 264 px

.. |IMG5| image:: _static/img/attribute_display_css_output.png
   :height: 105 px
   :width: 320 px
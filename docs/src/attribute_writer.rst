Attribute Writer Widget
**********************************

This widget enables the user to write predefined values to an attribute. It gives user a interface to provide input to override an attributes value.

Widget setting 
===============

From inspector panel user has the ability to:

- Show device name (boolean)
- Choose attribute display type (Label, Name or None)
- Align value on right (boolean)
- Set text color (string)
- Set background color (string)
- Set text size (number)
- Choose font type (Helvetica or Monospaced )
- Write custom CSS for the widget container.

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 

\*Notice: Depending on type of attribute selected, widget allows user to input numer, string, boolean or special characters.

Good to know about this widget:

- If user has inputed invalid value then a error message is shown signifying user to enter valid input. For example for ``DevDouble`` attribute, ``string`` is invalid input.

\ |IMG3|\ 

- For attribute of type boolean a dropdown is shown with values ``true`` & ``false``. A ``write`` button is shown to overwrite attributes value.
- For non boolean attributes, user can overwrite its value by pressing ``enter`` key in textbox.
- For non boolean attributes, until user types valid input for attribute ``enter`` is not consumed to overwrite attributes value.
- Any css rules defined under ``Custom CSS`` section would be applied to widget. This will help to customize the ``look and feel`` of the widget.

.. bottom of content

.. |IMG1| image:: _static/img/attribute_writer_inspector.png
   :height: 612 px
   :width: 293 px

.. |IMG2| image:: _static/img/attribute_writer_runtime.png
   :height: 148 px
   :width: 525 px

.. |IMG3| image:: _static/img/attribute_writer_invalid.png
   :height: 158 px
   :width: 525 px
Command Switch Widget
******************************************************************************************************************************************

The widget enables the user to create a switch to send a different command with parameters to a different devices based on its state 
User can also send arguments while sending commands. User can also customize the widget using image & CSS

Widget setting 
==============================================

The user as the hability to select:

* Title of Widget
* On State device & command
* Off State device & command
* Show device name (boolean)
* Show command name (boolean)
* Arguments for On command
* Arguments for Off command
* Select image to show for On and Off state of switch
* Apply CSS to widget container during On state
* Apply CSS to widget container during Off state
* Apply CSS to switch

\ |IMG1|\ 

Any web url of image can be used to customize On / Off state of switch
CSS applied to widget container & switch is similar to traditional CSS as shown below
color: green;
background-color: red;

\*Notice: For a command to be listed on the widget, the accepted parameters must be either a string or an integer.

Widget design 
==============================================

\ |IMG2|\ 

\*Notice: User clicks the switch button which fires command with given arguments to the device.
User can select select different device & command to represent On & Off state of switch which will be updated when user interacts / clicks switch

.. bottom of content

.. |IMG1| image:: _static/img/command_switch_setting.png
   :height: 350 px
   :width: 327 px

.. |IMG2| image:: _static/img/command_switch_widget.png
   :height: 45 px
   :width: 464 px
import React from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import configureStore from "./shared/state/store/configureStore";
import JiveApp from "./jive/App";
import DashboardApp from "./dashboard/App";
import SynopticApp from "./synoptic/App";
import { createRoot } from 'react-dom/client';
import swal from 'sweetalert';

import "bootstrap/dist/css/bootstrap.min.css";
const store = configureStore();
const config = window['config'];
const root = createRoot(document.getElementById('root'));

root.render(
  <Provider store={store}>
    <BrowserRouter basename={config.basename}>
      <Switch>
        <Route path={"/:tangoDB/dashboard"} component={DashboardApp} />
        <Route path={"/:tangoDB/synoptic"} component={SynopticApp} />
        <Route path={"/"} component={JiveApp} />
      </Switch>
    </BrowserRouter>
  </Provider>
);

window.onerror = function (message, source, lineno, colno, error) {
  // Create a user-friendly error message
  const errorData = {
    message: message,
    file: source,
    line: lineno,
    column: colno,
    errorStack: error ? error.stack : 'No stack trace'
  };

  const errorMessage = `Oops! Something went wrong 😢\n\n` +
                       `Error: ${JSON.stringify(errorData)}\n` +
                       `This occurred at line ${lineno} of the source file`;

  // Show the error message in an swal alert
  swal(errorMessage, {
    buttons: ["Copy error to Clipboard", "Exit"],
  }).then((value) => {
    console.log(value);
    switch (value) {
      case null:
        navigator.clipboard.writeText(JSON.stringify(errorData));
        swal("Copied to clipboard, please inform taranta developers about this error");
        break;
      default:
        break;
    }
  });
};

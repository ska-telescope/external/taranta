import React, { useEffect, useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import PubSub from "pubsub-js";
import SvgComponent from "../../../shared/components/SvgComponent/SvgComponent";
import { getSelectedSynoptic } from "../../state/selectors";
import { WEBSOCKET } from "../../../shared/state/actions/actionTypes";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { validateSVG } from "../../../dashboard/utils";
import Thumbnail from "../../../shared/components/SvgComponent/Thumbnail/Thumbnail";
interface Props {
  mode: "edit" | "run";
}

const SynopticDisplay: React.FC<Props> = ({ mode }) => {
  let synoptic = useSelector(getSelectedSynoptic);
  const dispatch = useDispatch();
  const [devices, setDevices] = useState<string[]>([]);
  const [shownDevices, setShownDevices] = useState<string[]>(devices);
  const deviceList = useSelector((state: IRootState) => {
    return state.deviceList;
  });
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
  const displayRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (synoptic.svg !== "" || synoptic.svg !== undefined) {
      const res = validateSVG(synoptic.svg, deviceList.nameList);
      setDevices(res.scannedDevices);
      if (synoptic.svg.match(/viewBox+/) == null) {
        const width = synoptic.svg.match(/width="\d.[^a-z]+/);
        const height = synoptic.svg.match(/height="\d.[^a-z]+/);
        if (width && height) {
          const widthValue = width[0].match(/\d.[^"]+/) || "";
          const heightValue = height[0].match(/\d.[^"]+/) || "";
          synoptic.svg =
            synoptic.svg.slice(0, height.index) +
            `viewBox="0 0 ${widthValue[0]} ${heightValue[0]}"\n   ` +
            synoptic.svg.slice(height.index);
        }
      }
    }
  }, [synoptic, deviceList.nameList]);

  useEffect(() => {
    if (mode === "run") {
      const uniqueDevices = shownDevices.filter(
        (device, index) => shownDevices.indexOf(device) === index
      );
      // Limit the subscribtion to the first 200 devices
      const restrictedDevices =
        uniqueDevices.length > 200
          ? uniqueDevices.slice(0, 200)
          : uniqueDevices;

      dispatch({
        type: WEBSOCKET.WS_SUBSCRIBE,
        payload: { devices: restrictedDevices },
      });
    }
  }, [mode, shownDevices, dispatch]);

  useEffect(() => {
    const handleResize = () => {
      if (displayRef.current) {
        setDimensions({
          width: displayRef.current.clientWidth,
          height: displayRef.current.clientHeight,
        });
      }
    };

    window.addEventListener("resize", handleResize);
    handleResize(); // Call it initially to set the dimensions

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    var token = PubSub.subscribe("current visibleNodes", (_, visibleNodes) => {
      setShownDevices(visibleNodes);
    });

    return () => {
      PubSub.unsubscribe(token);
    };
  }, []);

  return (
    <div className="SynopticDisplay" ref={displayRef}>
      <SvgComponent
        path={synoptic.svg}
        mode={mode}
        zoom={true}
        layers={true}
        svgCss={""}
        viewportSvgDimensions={dimensions}
      />
      <Thumbnail svgFile={synoptic.svg} viewportSvgDimensions={dimensions} />
    </div>
  );
};

export default SynopticDisplay;

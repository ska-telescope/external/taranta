import { Action } from "redux";
import {
  UNDO,
  REDO,
  DELETE_INPUT,
  ADD_INPUT,
  SET_INPUT,
  SELECT_CANVAS,
  TOGGLE_MODE,
  SYNOPTIC_LOADED,
  SYNOPTICS_LOADED,
  SYNOPTIC_RENAMED,
  SYNOPTIC_DELETED,
  SYNOPTIC_CLONED,
  SYNOPTIC_SAVED,
  SHOW_NOTIFICATION,
  HIDE_NOTIFICATION,
  SAVE_SYNOPTIC,
  RENAME_SYNOPTIC,
  CLONE_SYNOPTIC,
  SHARE_SYNOPTIC,
  SYNOPTIC_SHARED,
  SYNOPTIC_EDITED,
  TOGGLE_INSPECTOR_COLLAPSED,
  TOGGLE_LIBRARY_COLLAPSED,
  SYNOPTIC_EXPORTED,
  SYNOPTIC_IMPORTED,
} from "./actionTypes";

import { IndexPath, Synoptic, SelectedSynoptic, Notification, Variable } from "../types";
import { SelectedSynopticState } from "./reducers/selectedSynoptic";

export interface UndoAction extends Action {
  type: typeof UNDO;
}

export interface RedoAction extends Action {
  type: typeof REDO;
}

export interface SetInputAction extends Action {
  type: typeof SET_INPUT;
  path: IndexPath;
  value: any;
}

export interface AddInputAction extends Action {
  type: typeof ADD_INPUT;
  path: IndexPath;
}

export interface DeleteInputAction extends Action {
  type: typeof DELETE_INPUT;
  path: IndexPath;
}

export interface SelectCanvasAction extends Action {
  type: typeof SELECT_CANVAS;
  id: string;
}

export interface ToggleModeAction extends Action {
  type: typeof TOGGLE_MODE;
}

export interface PreloadSynopticAction extends Action {
  type: typeof SYNOPTIC_LOADED;
  id: string;
  name: string;
  user: string;
}

export interface SynopticsLoadedAction extends Action {
  type: typeof SYNOPTICS_LOADED;
  synoptics: Synoptic[];
}

export interface RenameSynopticAction extends Action {
  type: typeof RENAME_SYNOPTIC;
  id: string;
  name: string;
}

export interface SynopticRenamedAction extends Action {
  type: typeof SYNOPTIC_RENAMED;
  id: string;
  name: string;
}

export interface SynopticDeletedAction extends Action {
  type: typeof SYNOPTIC_DELETED;
  id: string;
}

export interface CloneSynopticAction extends Action {
  type: typeof CLONE_SYNOPTIC;
  id: string;
  newUser: string;
}
export interface SynopticClonedAction extends Action {
  type: typeof SYNOPTIC_CLONED;
  id: string;
}
export interface ShareSynopticAction extends Action{
  type: typeof SHARE_SYNOPTIC;
  id: string;
  group: string;
}

export interface SynopticSharedAction extends Action{
  type: typeof SYNOPTIC_SHARED;
  id: string;
  group: string;
  groupWriteAccess: boolean;
}

export interface SynopticLoadedAction extends Action {
  type: typeof SYNOPTIC_LOADED;
  synoptic: SelectedSynoptic;
}
export interface SynopticExportedAction extends Action {
  type: typeof SYNOPTIC_EXPORTED;
  synoptic: Synoptic;
}
export interface SynopticImportedAction extends Action {
  type: typeof SYNOPTIC_IMPORTED;
  synoptic: Synoptic;
}
export interface SaveSynopticAction extends Action {
  type: typeof SAVE_SYNOPTIC;
  id: string;
  name: string;
  variables: Variable[];
}

export interface SynopticSavedAction extends Action {
  type: typeof SYNOPTIC_SAVED;
  id: string;
  created: boolean;
  name: string;
  variables: Variable[];
}

export interface ShowNotificationAction extends Action {
  type: typeof SHOW_NOTIFICATION;
  notification: Notification;
}

export interface HideNotificationAction extends Action {
  type: typeof HIDE_NOTIFICATION;
  notification: Notification;
}

export interface synopticEditedAction extends Action {
  type: typeof SYNOPTIC_EDITED;
  synoptic: SelectedSynopticState

}

export interface ToggleInspectorCollapseAction extends Action {
  type: typeof TOGGLE_INSPECTOR_COLLAPSED;
}
export interface ToggleLibraryCollapseAction extends Action {
  type: typeof TOGGLE_LIBRARY_COLLAPSED;
}
export type SynopticAction =
  | UndoAction
  | RedoAction
  | SetInputAction
  | AddInputAction
  | DeleteInputAction
  | SelectCanvasAction
  | ToggleModeAction
  | SynopticLoadedAction
  | SynopticExportedAction
  | SynopticImportedAction
  | SynopticsLoadedAction
  | RenameSynopticAction
  | SynopticRenamedAction
  | SynopticDeletedAction
  | SynopticClonedAction
  | ShareSynopticAction
  | SynopticSharedAction
  | SynopticSavedAction
  | ShowNotificationAction
  | HideNotificationAction
  | CloneSynopticAction
  | SynopticClonedAction
  | synopticEditedAction
  | ToggleInspectorCollapseAction
  | ToggleLibraryCollapseAction

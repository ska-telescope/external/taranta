import { cloneableGenerator } from '@redux-saga/testing-utils';
import { fork, take, put, call } from 'redux-saga/effects';
import mongodb, { fetchLoggedActions } from './mongodb';
import MongoAPI from '../../../shared/api/mongoAPI';
import { FETCH_LOGGED_ACTIONS } from '../actions/actionTypes';
import {
  fetchLoggedActionsFailed,
  fetchLoggedActionsSuccess,
} from '../actions/mongodb';

describe('mongodb saga', () => {
  test('should fork fetchLoggedActions saga', () => {
    const gen = mongodb();
    expect(gen.next().value).toEqual(fork(fetchLoggedActions));
  });
});

describe('fetchLoggedActions saga', () => {
  test('should fetch logged actions and dispatch success action', () => {
    const tangoDB = 'testDB';
    const deviceName = 'testDevice';
    const limit = 100;
    const action = { type: FETCH_LOGGED_ACTIONS, tangoDB, deviceName, limit };
    const gen = cloneableGenerator(fetchLoggedActions)(action);

    expect(gen.next().value).toEqual(take(FETCH_LOGGED_ACTIONS));

    const logs = [{ action: 'testAction' }];
    expect(gen.next(action).value).toEqual(
      call(MongoAPI.fetchLoggedActions, tangoDB, deviceName, limit)
    );

    expect(gen.next(logs).value).toEqual(
      put(fetchLoggedActionsSuccess(logs))
    );

    const clone = gen.clone();
    const error = new Error('Failed to fetch logged actions');
    expect(clone.throw(error).value).toEqual(
      put(fetchLoggedActionsFailed(error.toString()))
    );

    expect(clone.next().value).toEqual(take(FETCH_LOGGED_ACTIONS));
  });
});

import { take, put, call, select } from "redux-saga/effects";
import { cloneableGenerator } from '@redux-saga/testing-utils';
import {
    loadDashboards,
    renameDashboard,
    deleteDashboard,
    exportDashboardSaga,
    importDashboardSaga,
    shareDashboard,
    cloneDashboard,
    notifyOnExport,
    saveDashboard,
    notifyOnSave,
    notifyOnShare,
    notifyOnClone,
    notifyOnDelete,
    updateDashboard,
} from "./dashboards";
import * as API from "../../../dashboard/dashboardRepo";
import {
    dashboardExported,
    showNotification,
    dashboardsLoaded,
    dashboardShared,
    dashboardRenamed,
    saveDashboard as saveDashboardAction,
    dashboardDeleted,
    dashboardCloned,
    dashboardUpdated,
} from "../actions/actionCreators";
import {
    resolveWidgetCompatibility,
} from "../reducers/selectedDashboard/lib";
import {
    EXPORT_DASHBOARD,
    LOAD_DASHBOARDS,
    DASHBOARD_RENAMED,
    DASHBOARD_DELETED,
    DASHBOARD_CLONED,
    DASHBOARD_SAVED,
    SHARE_DASHBOARD,
    RENAME_DASHBOARD,
    DELETE_DASHBOARD,
    CLONE_DASHBOARD,
    IMPORT_DASHBOARD,
    SHOW_NOTIFICATION,
    DASHBOARD_EXPORTED,
    SAVE_DASHBOARD,
    DASHBOARD_CREATED,
    DASHBOARD_SHARED,
    DASHBOARD_UPDATED,
    UPDATE_DASHBOARD
} from "../actions/actionTypes";
import { LOGIN_SUCCESS } from '../../user/state/actionTypes';
import { NotificationLevel } from "../../notifications/notifications";
import {
    getDashboards,
    getWidgets,
} from "../selectors";
import { getDeviceNames } from "../selectors/deviceList";

describe('exportDashboardSaga', () => {
    test('should export dashboard and dispatch dashboardExported action', () => {
        const id = 'testId';
        const action = { type: EXPORT_DASHBOARD, id };
        const gen = cloneableGenerator(exportDashboardSaga)(action);

        expect(gen.next().value).toEqual(take(EXPORT_DASHBOARD));

        const result = {
            widgets: [{
                id: 'testWidgetId',
                type: 'ATTRIBUTE_DISPLAY',
                inputs: {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "double_scalar",
                        "label": "double_scalar"
                    },
                    "precision": 2,
                    "showDevice": true,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "showAttrQuality": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica",
                    "widgetCss": ""
                }
            }],
            name: 'testDashboardName',
            user: 'testUser',
            insertTime: new Date(),
            updateTime: new Date(),
            group: 'testGroup',
            lastUpdatedBy: 'testUser',
            variables: {},
        };
        const { widgets, name, user, insertTime, updateTime, group, groupWriteAccess, lastUpdatedBy, variables } = result;

        expect(gen.next(action).value).toEqual(call(API.exportDash, id));

        const { widgets: newWidgets } = resolveWidgetCompatibility(widgets);

        expect(gen.next(result).value).toEqual(put(dashboardExported({ id, name, user, insertTime, updateTime, group, groupWriteAccess, lastUpdatedBy, variables }, newWidgets)));

        const clone = gen.clone();
        const error = new Error('Failed to export dashboard');
        expect(clone.throw(error).value).toEqual(
            put(showNotification(NotificationLevel.ERROR, EXPORT_DASHBOARD, 'Dashboard not exported: ' + error))
        );

        expect(clone.next().value).toEqual(take(EXPORT_DASHBOARD));
    });
});

describe('loadDashboards saga', () => {
    test('should load dashboards and dispatch dashboardsLoaded action', () => {
        const gen = cloneableGenerator(loadDashboards)();
        expect(gen.next().value).toEqual({
            '@@redux-saga/IO': true,
            combinator: false,
            type: 'TAKE',
            payload: {
                pattern: [
                    LOAD_DASHBOARDS,
                    LOGIN_SUCCESS,
                    DASHBOARD_RENAMED,
                    DASHBOARD_DELETED,
                    DASHBOARD_CLONED,
                    DASHBOARD_SAVED,
                    DASHBOARD_UPDATED, 
                    DASHBOARD_SHARED
                ],
            }
        }
        );

        const payload = { type: LOAD_DASHBOARDS };
        expect(gen.next(payload).value).toEqual(call(API.loadUserDashboards));

        const data = [
            {
                "id": "66e94d5d3fb10e0012c19d73",
                "name": "Spectrum Index",
                "user": "user1",
                "environment": [
                    "*/*"
                ],
                "insertTime": "2024-09-17T09:35:25.663Z",
                "updateTime": "2024-09-24T10:18:50.714Z",
                "group": null,
                "lastUpdatedBy": "user1",
                "tangoDB": "testdb",
                "variables": []
            }
        ];
        expect(gen.next(data).value).toEqual(put(dashboardsLoaded(data)));

        const clone = gen.clone();
        const error = new Error('Failed to load dashboards');
        expect(clone.throw(error).done).toEqual(false);
    });
});

describe('shareDashboard saga', () => {
    test('should share dashboard and dispatch dashboardShared action', () => {
        const gen = cloneableGenerator(shareDashboard)();

        expect(gen.next().value).toEqual(take(SHARE_DASHBOARD));

        const payload = { id: 'testDashboardId', group: 'testGroup', groupWriteAccess: true };

        expect(gen.next(payload).value).toEqual(call(API.shareDashboard, 'testDashboardId', 'testGroup', true));

        expect(gen.next().value).toEqual(put(dashboardShared('testDashboardId', 'testGroup', true)));

        const clone = gen.clone();
        const error = new Error('Failed to share dashboard');
        try {
            clone.throw(error);
        } catch (e) {
            expect(e).toBe(error);
        }
        expect(clone.next().done).toBe(true);
    });
});

describe('renameDashboard saga', () => {
    test('should rename dashboard and dispatch dashboardRenamed action', () => {
        const gen = cloneableGenerator(renameDashboard)();
        expect(gen.next().value).toEqual(take(RENAME_DASHBOARD));

        const payload = { id: 'testDashboardId', name: 'Test Dashboard Name', environment: ['*/*'] };
        expect(gen.next(payload).value).toEqual(select(getDashboards));
    
        const mockDashboards = [{ id: 'someId', name: 'Some Dashboard', environment: ['*/*'] }];
        expect(gen.next(mockDashboards).value).toEqual(
            call(API.renameDashboard, payload.id, payload.name, payload.environment)
        );
        expect(gen.next().value).toEqual(put(dashboardRenamed('testDashboardId', 'Test Dashboard Name')));
    
        const clone = gen.clone();
        expect(clone.next().value).toEqual(take(RENAME_DASHBOARD));
    
        const emptyIdPayload = { id: '', name: 'Test Dashboard Name' };
        expect(clone.next(emptyIdPayload).value).toEqual(select(getWidgets));
    
        const widgets = {};
        expect(clone.next(widgets).value).toEqual(put(saveDashboardAction('', 'Test Dashboard Name', widgets)));
    });

    test('should catch errors thrown by API.renameDashboard', () => {
        const gen = cloneableGenerator(renameDashboard)();
        expect(gen.next().value).toEqual(take(RENAME_DASHBOARD));
    
        const payload = { id: 'testDashboardId', name: 'Test Dashboard Name', environment: ['*/*'] };
        expect(gen.next(payload).value).toEqual(select(getDashboards));
    
        const mockDashboards = [{ id: 'someId', name: 'Some Dashboard' }];    
        expect(gen.next(mockDashboards).value).toEqual(
            call(API.renameDashboard, payload.id, payload.name, payload.environment)
        );
    });
});

describe('deleteDashboard saga', () => {
    test('should delete dashboard and dispatch dashboardDeleted action', () => {
        const gen = cloneableGenerator(deleteDashboard)();

        expect(gen.next().value).toEqual(take(DELETE_DASHBOARD));

        const payload = { id: 'testDashboardId' };

        expect(gen.next(payload).value).toEqual(call(API.deleteDashboard, 'testDashboardId'));

        expect(gen.next({ id: 'testDashboardId' }).value).toEqual(put(dashboardDeleted('testDashboardId')));
    });
});

describe('cloneDashboard saga', () => {
    test('should clone dashboard and dispatch dashboardCloned action', () => {
        const gen = cloneableGenerator(cloneDashboard)();

        expect(gen.next().value).toEqual(take(CLONE_DASHBOARD));

        const payload = { id: 'testDashboardId' };

        expect(gen.next(payload).value).toEqual(call(API.cloneDashboard, 'testDashboardId'));

        expect(gen.next({ id: 'newTestDashboardId' }).value).toEqual(put(dashboardCloned('newTestDashboardId')));
    });
});

describe('importDashboardSaga', () => {
    test('should import dashboard and dispatch dashboardSaved action', () => {
        const gen = cloneableGenerator(importDashboardSaga)();    
        expect(gen.next().value).toEqual(take(IMPORT_DASHBOARD));
    
        const payload = { content: 'testDashboardContent' };
        expect(gen.next(payload).value).toEqual(select(getDashboards));
    
        const mockedDashboards = []; 
        expect(gen.next(mockedDashboards).value).toEqual(select(getDeviceNames));
    
        const mockedDeviceList = [];
        expect(gen.next(mockedDeviceList).value).toEqual(call(API.importDash, payload.content, mockedDashboards, mockedDeviceList));
    
        const mockedResult = {
            created: true,
            id: 'newTestDashboardId',
            dateCreated: '2022-04-01T12:00:00Z', 
            name: 'New Test Dashboard'
        };
    
        jest.spyOn(API, 'importDash').mockReturnValue(mockedResult);
    
        expect(gen.next(mockedResult).value.payload.action.type).toEqual(SHOW_NOTIFICATION);
    
        API.importDash.mockRestore();
    });    
});
  

describe("notifyOnExport saga", () => {
    test("should notify when a dashboard is exported", () => {
        const gen = cloneableGenerator(notifyOnExport)();
        const dashboard = { id: "testDashboardId" };
        const action = { type: DASHBOARD_EXPORTED, dashboard };
        const notificationAction = showNotification(
            NotificationLevel.INFO,
            DASHBOARD_EXPORTED,
            "Dashboard exported"
        );

        expect(gen.next(dashboard).value).toEqual(take(DASHBOARD_EXPORTED));
        expect(gen.next(action).value).toEqual(
            put(notificationAction)
        );
    });
});

describe("saveDashboard saga", () => {
    test("should save dashboard and dispatch dashboardSaved action", () => {
        const gen = cloneableGenerator(saveDashboard)();
        
        expect(gen.next().value).toEqual(take('SAVE_DASHBOARD'));
    
        const mockPayload = {
            id: "testDashboardId",
            widgets: [],
            name: "New Dashboard 2",
            variables: {},
            environment: ['*/*']
        };
    
        expect(gen.next(mockPayload).value).toEqual(select(getDashboards));
        
        const dashboards = [{ id: 'someId', name: 'Some Dashboard' }];
    
        expect(gen.next(dashboards).value).toEqual(
            call(API.save, mockPayload.id, mockPayload.widgets, mockPayload.name, mockPayload.variables, mockPayload.environment)
        );
    
        const apiResponse = {
            id: 'newTestDashboardId',
            created: 'someDate'
        };
    
        const expectedPutAction = {
            type: 'DASHBOARD_SAVED',
            id: apiResponse.id,  
            name: 'New Dashboard 2',
            created: apiResponse.created,  
            variables: {},
            environment: ['*/*']
        };
    
        expect(gen.next(apiResponse).value).toEqual(put(expectedPutAction));
    });

    test("should show an error notification if the dashboard cannot be saved", () => {
        const gen = saveDashboard();
        
        expect(gen.next().value).toEqual(take(SAVE_DASHBOARD));
    
        const mockPayload = {
            id: "testDashboardId",
            widgets: [],
            name: "Existing Dashboard",
            variables: {},
            environment: ['*/*']
        };
        expect(gen.next(mockPayload).value).toEqual(select(getDashboards));
    
        const mockDashboards = [{ id: 'someId', name: 'Existing Dashboard' }];        
        expect(gen.next(mockDashboards).value).toEqual(
            call(API.save, mockPayload.id, mockPayload.widgets,
                "Existing Dashboard 2", mockPayload.variables, mockPayload.environment)
        );
    });
});

describe("notifyOnSave saga", () => {
    test("should show info notification on dashboard create", () => {
        const gen = cloneableGenerator(notifyOnSave)();

        expect(gen.next().value).toEqual(take(DASHBOARD_SAVED));

        const payload = { created: true };

        expect(gen.next(payload).value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_CREATED,
                    "Dashboard created"
                )
            )
        );

        expect(gen.next().value).toEqual(take(DASHBOARD_SAVED));
    });
});


describe("notifyOnShare saga", () => {
    test("should show info notification on dashboard share", () => {
        const gen = cloneableGenerator(notifyOnShare)();

        expect(gen.next().value).toEqual(take(DASHBOARD_SHARED));

        const payload = { group: 'testGroup', groupWriteAccess: false };

        expect(gen.next(payload).value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_SHARED,
                    "Dashboard shared with testGroup"
                )
            )
        );

        expect(gen.next().value).toEqual(take(DASHBOARD_SHARED));
    });
});

describe("notifyOnClone saga", () => {
    test("should show info notification on dashboard clone", () => {
        const gen = cloneableGenerator(notifyOnClone)();

        expect(gen.next().value).toEqual(take(DASHBOARD_CLONED));

        expect(gen.next().value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_CLONED,
                    "Dashboard cloned"
                )
            )
        );

        expect(gen.next().value).toEqual(take(DASHBOARD_CLONED));
    });
});

describe("notifyOnDelete saga", () => {
    test("should show info notification on dashboard delete", () => {
        const gen = cloneableGenerator(notifyOnDelete)();

        expect(gen.next().value).toEqual(take(DASHBOARD_DELETED));

        expect(gen.next().value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_DELETED,
                    "Dashboard deleted"
                )
            )
        );
    });
});

describe("updateDashboard saga", () => {
    test("should updateDashboard", () => {
        const gen = cloneableGenerator(updateDashboard)();
        expect(gen.next().value).toEqual(take(UPDATE_DASHBOARD));

        let payload = {
            id: "123",
            fields: {
                environment: ["*/*"]
            }
        }

        expect(gen.next(payload).value).toEqual(
            call(API.updateDashboard, "123", {environment: ["*/*"]})
        );
        expect(gen.next().value).toEqual(select(getDeviceNames));


        const deviceList = ['device1', 'device2'];
        expect(gen.next(deviceList).value).toEqual(
            put(dashboardUpdated(payload.id, payload.fields, deviceList))
        );

        expect(gen.next().value).toEqual(take(UPDATE_DASHBOARD));
    });

    test("updateDashboard error case", () => {
        const gen = cloneableGenerator(updateDashboard)();
        expect(gen.next().value).toEqual(take(UPDATE_DASHBOARD));

        let payload = {
            id: "123",
            fields: {
                environment: ["*/*"]
            }
        }

        expect(gen.next(payload).value).toEqual(
            call(API.updateDashboard, "123", {environment: ["*/*"]})
        );
        expect(gen.next().value).toEqual(select(getDeviceNames));

        const clone = gen.clone();
        const error = new Error('Test error');
        expect(clone.throw(error).value).toEqual(
            put(showNotification(NotificationLevel.ERROR, UPDATE_DASHBOARD, 'Failed to save dashboard: ' + error.message))
        );
    });
});

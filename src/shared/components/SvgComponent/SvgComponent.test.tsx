import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import SvgComponent from "./SvgComponent";
import configureStore from "redux-mock-store";
import {SVG} from "./svgTest"

const store = configureStore([]);

const mockStore = store({
    dispatch: jest.fn(),
    deviceList: {
        "nameList": [
            "testdb://sys/tg_test/1"
            ],
        "filter": ""
    },
    messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: "RUNNING",
              quality: ["ATTR_VALID"],
              timestamp: 123
            }
          }
        }
      }
  });
let props = {
    path: "<svg></svg>",
    mode: "edit",
    zoom: false,
    layers: false,
    svgCss: "",
    viewportSvgDimensions: { width: 0, height: 0 },
};

describe("SvgComponent", () => {
  it("renders without crashing", () => {

    props.mode = "run";

    const element = render(
        <Provider store={mockStore}>
          <SvgComponent {...props} />
        </Provider>
      ).baseElement;

    expect(element.innerHTML).toContain('class="SvgComponent"');
  });

  it("doesn't render svg", () => {

    props.mode = "run";

    const element = render(
        <Provider store={mockStore}>
          <SvgComponent {...props} />
        </Provider>
      ).baseElement;

    expect(element.innerHTML).not.toContain('state');
    expect(element.innerHTML).not.toContain('ellipse');
  });

  it("show correct message if no devices are present", () => {

    const noDeviceMockStore = store({
      dispatch: jest.fn(),
      deviceList: [],
      messages: {
          tg_test: {
            attributes: {
              attribute1: {
                values: "RUNNING",
                quality: ["ATTR_VALID"],
                timestamp: 123
              }
            }
          }
        }
    });
    const element = render(
        <Provider store={noDeviceMockStore}>
          <SvgComponent {...props} />
        </Provider>
      ).baseElement;
    expect(element.innerHTML).toContain('Waiting for the list of existing devices');
  });

  it("render state", () => {
    props.path = SVG;

    const stateMockStore = store({
      dispatch: jest.fn(),
      deviceList: {
        "nameList": [
            "testdb://sys/tg_test/1"
            ],
        "filter": ""
    },
      messages: {
          tg_test: {
            attributes: {
              state: {
                values: "RUNNING",
                quality: ["ATTR_VALID"],
                timestamp: 123
              }
            }
          }
        }
    });
    const element = render(
        <Provider store={stateMockStore}>
          <SvgComponent {...props} />
        </Provider>
      ).baseElement;

    expect(element.innerHTML).toContain('model=sys/tg_test/1');
    expect(element.innerHTML).toContain('state');
  });

  it("test extractAttributeFromRules", () => {
    const rules = [
      {
        "type": "layer",
        "model": "sys/tg_test/1/short_scalar",
        "layer": "star",
        "default": "hide",
        "condition": "value>21 and value<90"
      },
      {
        "type": "css",
        "model": "sys/tg_test/1/short_scalar",
        "ifcss": "{\"stroke\":\"orange\"}",
        "elsecss": "{\"stroke\":\"purple\"}",
        "condition": "value>31"
      }
    ]
  })
});

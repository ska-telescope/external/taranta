import React from "react";
import { render, fireEvent, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import PubSub from "pubsub-js";
import Thumbnail from "./Thumbnail";
jest.mock("pubsub-js", () => ({
  subscribe: jest.fn(),
  unsubscribe: jest.fn(),
  publish: jest.fn(),
}));
const fs = require("fs");
const svgFile = fs
  .readFileSync("src/synoptic/testingUtils/testSVGfile.svg")
  .toString();
let myFile = {
  svg: svgFile,
  selectedId: 1,
  selectedIds: [],
  id: "",
  name: "Untitled synoptic",
  user: "",
  group: "",
  groupWriteAccess: false,
  lastUpdatedBy: "",
  insertTime: null,
  updateTime: null,
  history: {
    undoActions: [],
    redoActions: [],
    undoIndex: 0,
    redoIndex: 0,
    undoLength: 0,
    redoLength: 0,
  },
  variables: [],
};
let dimensions = { width: 10, height: 10 };

describe("Thumbnail", () => {
  beforeEach(() => {
    PubSub.subscribe.mockReturnValue({ x: 0, y: 0, scale: 0.5 });
  });
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render the thumbnail", () => {
    const { container } = render(
      <Thumbnail svgFile={myFile.svg} viewportSvgDimensions={dimensions} />
    );
    expect(container.innerHTML).toContain("thumbnail");
    expect(container.innerHTML).toContain("indicator");
    expect(container.innerHTML).toContain("thumbnail-toggle");
  });

  it("hide the thumbnail by toggling the visibility", () => {
    const { container } = render(
      <Thumbnail svgFile={myFile.svg} viewportSvgDimensions={dimensions} />
    );
    const toggleButton = screen.getByTitle("Hide/show thumbnail");
    fireEvent.click(toggleButton);
    expect(toggleButton).toHaveTextContent("-");
  });

  it("Pan to specific area if clicking the thumbnail", async () => {
    waitFor(() => {
      const thumbNail = render(
        <Thumbnail svgFile={myFile.svg} viewportSvgDimensions={dimensions} />
      );
      const button = thumbNail.getByTitle("synoptic");
      fireEvent.click(button);
      expect(PubSub.publish).toHaveBeenCalled();
    });
    expect(PubSub.subscribe).toHaveBeenCalled();
  });

  it("unsubscribe when unmounting the component", async () => {
    const { unmount } = render(
      <Thumbnail svgFile={myFile.svg} viewportSvgDimensions={dimensions} />
    );
    unmount();
    expect(PubSub.unsubscribe).toHaveBeenCalled();
  });
});

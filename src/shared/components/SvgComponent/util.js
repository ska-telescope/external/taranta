var util = {};

(function() {
  // Calculate the bounding box of an element with respect to its parent element
  util.transformedBoundingBox = function(el) {
    var bb = el.getBBox(),
      svg = el.ownerSVGElement ? el.ownerSVGElement : el;
    try {
      var m = el.transform.baseVal.consolidate().matrix;
    } catch (e) {
      m = svg.createSVGMatrix();
    }

    // Create an array of all four points for the original bounding box
    var pts = [
      svg.createSVGPoint(),
      svg.createSVGPoint(),
      svg.createSVGPoint(),
      svg.createSVGPoint(),
    ];
    pts[0].x = bb.x;
    pts[0].y = bb.y;
    pts[1].x = bb.x + bb.width;
    pts[1].y = bb.y;
    pts[2].x = bb.x + bb.width;
    pts[2].y = bb.y + bb.height;
    pts[3].x = bb.x;
    pts[3].y = bb.y + bb.height;

    // Transform each into the space of the parent,
    // and calculate the min/max points from that.
    var xMin = Infinity,
      xMax = -Infinity,
      yMin = Infinity,
      yMax = -Infinity;
    pts.forEach(function(pt) {
      pt = pt.matrixTransform(m);
      xMin = Math.min(xMin, pt.x);
      xMax = Math.max(xMax, pt.x);
      yMin = Math.min(yMin, pt.y);
      yMax = Math.max(yMax, pt.y);
    });

    // Update the bounding box with the new values
    bb.x = xMin;
    bb.width = xMax - xMin;
    bb.y = yMin;
    bb.height = yMax - yMin;
    return bb;
  };

  // set/unset a given class on an SVG element
  util.setClass = function(node, className, on) {
    var classes = node.getAttribute("class");
    var classList = (classes || "").split(" ");
    var index = classList.indexOf(className);
    if (!on && index === -1) {
      node.setAttribute("class", classes + " " + className);
    } else if (on && index !== -1) {
      classList.splice(index, 1);
      node.setAttribute("class", classList.join(" "));
    }
  };
})();

export default util;

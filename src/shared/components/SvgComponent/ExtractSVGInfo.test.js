const { JSDOM } = require('jsdom');
const { extractSVGInfo, getConditionalLayers, stringToBoolean, evaluateExpression } = require('./ExtractSVGInfo');

describe('extractSVGInfo', () => {
  let svgNode;

  beforeEach(() => {
    const { document } = (new JSDOM(`<!DOCTYPE html>
      <svg height="100%" width="100%" text-align="center" class="injected-svg" sodipodi:docname="testSVG.svg" inkscape:version="1.3.2 (1:1.3.2+202311252150+091e20ef0f)" id="svg1" version="1.1" viewBox="0 0 158.75 158.75" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
        <style xmlns="http://www.w3.org/2000/svg" id="style1">.MAXIV {   }
        .myclass {   }
        </style>
        <sodipodi:namedview xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" id="namedview1" pagecolor="#ffffff" bordercolor="#000000" borderopacity="0.25" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" inkscape:showpageshadow="2" inkscape:pageopacity="0.0" inkscape:pagecheckerboard="0" inkscape:deskcolor="#d1d1d1" inkscape:document-units="mm" inkscape:zoom="1.4627806" inkscape:cx="294.64432" inkscape:cy="250.2084" inkscape:window-width="1647" inkscape:window-height="864" inkscape:window-x="0" inkscape:window-y="0" inkscape:window-maximized="1" inkscape:current-layer="layer1"></sodipodi:namedview>
        <defs xmlns="http://www.w3.org/2000/svg" id="defs1"></defs>
        <g xmlns="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" inkscape:label="main" inkscape:groupmode="layer" id="layer1">
          <g inkscape:groupmode="layer" id="layer3" inkscape:label="Layer2" style="display:inline">
            <g inkscape:groupmode="layer" id="layer5" inkscape:label="zoom1">
              <ellipse style="display:inline;fill:#000080;stroke-width:0.600001" id="path1" cx="77.507004" cy="78.274399" rx="6.3949671" ry="6.1391683" class="model">
                <desc id="desc2">model=taranta/test/2</desc>
              </ellipse>
              <text xml:space="preserve" id="text2" style="display:inline;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:2.97236px;font-family:sans-serif;-inkscape-font-specification:'sans-serif, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-variant-east-asian:normal;fill:#241c1c;stroke-width:0.600001" x="51.688377" y="69.020653" class="MAXIV model">
                <desc id="desc3">model=MyVar1/Int_RO_001
                format=%.2
                format=%d</desc><tspan xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" sodipodi:role="line" id="tspan3" x="51.688377" y="69.020653">69 @2024-06-18 08:15:50.941022</tspan></text>
            </g>
          </g>
          <g inkscape:groupmode="layer" id="layer2" inkscape:label="Layer1" style="display:inline">
            <g inkscape:groupmode="layer" id="layer4" inkscape:label="zoom0" style="mix-blend-mode:normal">
              <rect style="display:inline;fill:#ff0000;stroke-width:0.600001" id="rect1" width="12.79" height="13.813128" x="71.11203" y="71.11203" class="myclass MAXIV model">
                <desc id="desc1">model=MyVar1</desc>
              </rect>
            </g>
          </g>
          <rect style="fill:none;stroke-width:0.600001" id="rect4" width="36.323414" height="19.696499" x="45.532166" y="-42.71838" inkscape:label="rules" data-model="testdb://sys/tg_test/1/ampli" data-tooltip-id="svg-tooltip" data-tooltip-float="true" data-tooltip-offset="15" class="model">
            <desc id="desc5">rule1: {
            type=layer
            model=sys/tg_test/1/temp
            layer=Layer2
            default=hide
            condition="temp &gt; 50"
            }
            rule2: {
            model=sys/tg_test/1/ampli
            layer=Layer3
            default=hide
            condition="ampli &gt; 3"
            }</desc>
          </rect>
        </g>
      </svg>`)).window;
    svgNode = document.querySelector('svg');
  });

  test('extracts rules correctly', () => {
    const { rules } = extractSVGInfo(svgNode);
    expect(rules).toEqual([
      {
        type: 'layer',
        model: 'sys/tg_test/1/temp',
        layer: 'Layer2',
        default: 'hide',
        condition: 'temp > 50'
      },
      {
        model: 'sys/tg_test/1/ampli',
        layer: 'Layer3',
        default: 'hide',
        condition: 'ampli > 3'
      }
    ]);

    const res = getConditionalLayers(rules);
    expect(res).toEqual(['Layer2']);

    //check for empty rules
    const res1 = getConditionalLayers();
    expect(res1).toEqual([]);
  });

  test('extracts components correctly', () => {
    const { components } = extractSVGInfo(svgNode);
    expect(components).toEqual(
      {
        "text2": { "format": ["%.2", "%d"] }
      },
    );
  });

  test('returns both components and rules', () => {
    const { components, rules } = extractSVGInfo(svgNode);
    expect(Object.values(components)).toHaveLength(1);
    expect(rules).toHaveLength(2);
  });

  test('test stringToBoolean', () => {
    expect(stringToBoolean("true")).toEqual(true)
    expect(stringToBoolean("false")).toEqual(false)
    expect(stringToBoolean(false)).toEqual(false)
  });

  test('test evaluateExpression', () => {
    expect(evaluateExpression("value>21", {value: "22"})).toEqual(true)
    expect(evaluateExpression("value==true", {value: "true"})).toEqual(true)
    expect(evaluateExpression("value>22 and value<50", {value: "44"})).toEqual(true)
    expect(evaluateExpression("value>22 and value<50", {value: "54"})).toEqual(false)
  });

});
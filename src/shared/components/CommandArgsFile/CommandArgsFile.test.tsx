import React from "react";
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from "@testing-library/react";
import "@testing-library/jest-dom";
import CommandArgsFile from "./CommandArgsFile";
import * as commandInputValidation from "../../utils/commandInputValidation";

jest.mock("react-redux", () => ({
  connect: () => (Component) => Component,
}));

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
  connect: () => (Component) => Component,
  setState: jest.fn(),
}));

jest.mock("../CommandOutput/CommandOutput", () => () => (
  <div data-testid="mocked-command-output" />
));

jest.mock("./DraggableModal", () => () => (
  <div data-testid="mocked-draggable-modal" />
));

jest.mock("../../../dashboard/dashboardRepo", () => ({
  getTangoDB: jest.fn(),
}));

jest.mock("../../user/state/selectors", () => ({
  getIsLoggedIn: jest.fn(),
}));

jest.mock("../../utils/commandInputValidation", () => ({
  ...jest.requireActual("../../utils/commandInputValidation"),
  validateCommandInput: jest.fn(),
}));

describe("CommandArgsFile core component", () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("render widget with file content", async () => {
    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={jest.fn()}
        loggedIn={true}
        displayOutput={true}
      />
    );

    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["Hello World"], "f1.txt", { type: "text/plain" });

    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });

    const fileNameDisplay = screen.getByText("f1.txt");
    expect(fileNameDisplay).toBeInTheDocument();
  });

  it("render widget with show alert, bad type", async () => {
    window.alert = jest.fn();
    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevShort",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={jest.fn()}
        loggedIn={true}
        displayOutput={true}
      />
    );

    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["Hello World"], "f1.txt", { type: "text/plain" });

    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });
    const fileNameDisplay = screen.getByText("f1.txt");
    expect(fileNameDisplay).toBeInTheDocument();

    const sendButton = screen.getByTitle("Click this button to fire command");
    await act(async () => {
      fireEvent.click(sendButton);
    });
    waitFor(() => {
      expect(window.alert).toHaveBeenCalled();
    });
  });
});
describe("CommandArgsFile Component", () => {
  class MockFileReader {
    onload: (event: any) => void = () => {};
    onerror: () => null = () => null;

    readAsText = () => {
      this.onload({ target: { result: "fileContent" } });
    };

    static EMPTY = 0;
    static LOADING = 1;
    static DONE = 2;
  }

  (global as any).FileReader = MockFileReader;

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("onFilesChange method works correctly", async () => {
    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={jest.fn()}
        loggedIn={true}
        displayOutput={true}
      />
    );

    let fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    let file = new File(["Hello World"], "f1.txt", { type: "text/plain" });
    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });
    let fileNameDisplay = screen.getByText("f1.txt");
    expect(fileNameDisplay).toBeInTheDocument();

    fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    file = new File([], "f2.json", { type: "application/json" });

    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });
    fileNameDisplay = screen.getByText("f2.json");
    expect(fileNameDisplay).toBeInTheDocument();
  });

  it("onFilesError method works correctly", async () => {
    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={jest.fn()}
        loggedIn={true}
        displayOutput={true}
      />
    );

    let fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const unsupportedFile = new File([""], "unsupported-file.mpg", {
      type: "audio/mpeg",
    });

    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [unsupportedFile] } });
    });
    waitFor(() => {
      expect(screen.getByText(/unsupported-file.mpg/)).toBeInTheDocument();
    });
    waitFor(() => {
      const openModalButton = screen.getByText(/Open Modal/);
      fireEvent.click(openModalButton);
      expect(screen.getByText(/unique modal content/)).toBeInTheDocument();
    });
  });
});

describe("handleSendCommand", () => {
  let originalConfirm;

  beforeEach(() => {
    originalConfirm = window.confirm;
  });

  afterEach(() => {
    window.confirm = originalConfirm;
  });

  it("does nothing when pending is true", async () => {
    const mockOnExecute = jest.fn();

    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={mockOnExecute}
        loggedIn={true}
        displayOutput={true}
      />
    );
    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["content"], "test.txt", { type: "text/plain" });
    fireEvent.change(fileInput, { target: { files: [file] } });

    waitFor(() => expect(screen.getByText(/Send:/)).not.toBeDisabled());

    const sendButton = screen.getByText(/Send:/);

    await act(async () => {
      fireEvent.click(sendButton);
    });

    waitFor(() => expect(mockOnExecute).toHaveBeenCalledTimes(1));

    await act(async () => {
      fireEvent.click(sendButton);
    });

    waitFor(() => expect(mockOnExecute).toHaveBeenCalledTimes(1));
  });

  it("shows alert when validateCommandInput returns undefined", async () => {
    const mockValidateCommandInput = commandInputValidation.validateCommandInput as jest.Mock;
    mockValidateCommandInput.mockReturnValueOnce(undefined);

    window.alert = jest.fn();

    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={jest.fn()}
        loggedIn={true}
        displayOutput={true}
      />
    );

    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["content"], "test.txt", { type: "text/plain" });
    fireEvent.change(fileInput, { target: { files: [file] } });

    waitFor(() => expect(screen.getByText(/Send:/)).not.toBeDisabled());

    const sendButton = screen.getByText(/Send:/);
    await act(async () => {
      fireEvent.click(sendButton);
    });
    waitFor(() => {
      expect(window.alert).toHaveBeenCalledWith(expect.any(String));
    });
  });

  it("executes command when requireConfirmation is true and user confirms", async () => {
    const mockOnExecute = jest.fn();
    window.confirm = jest.fn().mockReturnValue(true);
    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={true}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={mockOnExecute}
        loggedIn={true}
        displayOutput={true}
      />
    );

    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["content"], "test.txt", { type: "text/plain" });

    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });
    waitFor(() => expect(screen.getByText(/Send:/)).not.toBeDisabled());

    const sendButton = screen.getByText(/Send:/);
    await act(async () => {
      fireEvent.click(sendButton);
    });
    waitFor(() => {
      expect(mockOnExecute).toHaveBeenCalledWith(
        expect.anything(),
        expect.anything(),
        expect.anything(),
        expect.anything()
      );
    });
  });

  it("does not execute command when requireConfirmation is true and user cancels", async () => {
    const mockOnExecute = jest.fn();

    window.confirm = jest.fn().mockReturnValue(false);

    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={true}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={mockOnExecute}
        loggedIn={true}
        displayOutput={true}
      />
    );

    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["content"], "test.txt", { type: "text/plain" });
    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });
    waitFor(() => expect(screen.getByText(/Send:/)).not.toBeDisabled());
    const sendButton = screen.getByText(/Send:/);
    await act(async () => {
      fireEvent.click(sendButton);
    });
    waitFor(() => {
      expect(mockOnExecute).not.toHaveBeenCalled();
    });
  });

  it("executes command without confirmation when requireConfirmation is false", async () => {
    const mockOnExecute = jest.fn();
    const { container } = render(
      <CommandArgsFile
        tangoDB="testdb"
        label="sys/tg_test/1/DevString"
        uploadBtnCss={{}}
        uploadBtnLabel="Upload Button"
        sendBtnCss={{}}
        sendBtnText="Send"
        mode="run"
        requireConfirmation={false}
        alignSendButtonRight={true}
        commands={[]}
        command={{
          device: "sys/tg_test/1",
          command: "DevString",
          acceptedType: "DevString",
          intypedesc: "-",
          outtypedesc: "-",
          outtype: "DevString",
          tag: "0",
          output: [null],
          execute: jest.fn(),
        }}
        commandFromDevice={undefined}
        commandName="DevString"
        commandDevice="sys/tg_test/1"
        outerDivCss={{}}
        commandOutputs={{}}
        onExecute={mockOnExecute}
        loggedIn={true}
        displayOutput={true}
      />
    );

    const fileInput = container.querySelector(
      'input[type="file"]'
    ) as HTMLInputElement;
    const file = new File(["content"], "test.txt", { type: "text/plain" });
    await act(async () => {
      fireEvent.change(fileInput, { target: { files: [file] } });
    });
    waitFor(() => expect(screen.getByText(/Send:/)).not.toBeDisabled());
    const sendButton = screen.getByText(/Send:/);
    await act(async () => {
      fireEvent.click(sendButton);
    });
    waitFor(() => {
      expect(mockOnExecute).toHaveBeenCalledWith(
        expect.anything(),
        expect.anything(),
        expect.anything(),
        expect.anything()
      );
    });
  });
});

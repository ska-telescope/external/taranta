import tangoAPI from '../api/tangoAPI';
import { fetchInitialValues } from './fetchInitialValues';
import { WEBSOCKET } from "../../shared/state/actions/actionTypes";

jest.mock('../api/tangoAPI');

describe('fetchInitialValues', () => {
  it('dispatches a WEBSOCKET.WS_MESSAGE action with the fetched attributes', async () => {
    const dispatch = jest.fn();
    const element = 'test_device/test_attribute';
    const attributes = [
      {
        device: 'test_device',
        name: 'test_attribute',
        value: '10',
        timestamp: '2022-01-01T00:00:00.000Z',
        quality: 'VALID',
        writevalue: '0',
      },
    ];
    tangoAPI.fetchAttributesValues.mockResolvedValue(attributes);

    await fetchInitialValues(dispatch, element);

    expect(dispatch).toHaveBeenCalledTimes(1);

    expect(dispatch).toHaveBeenCalledWith({
      type: WEBSOCKET.WS_MESSAGE,
      value: [
        {
          data: JSON.stringify({
            payload: {
              data: {
                attributes: {
                  device: 'test_device',
                  attribute: 'test_attribute',
                  value: '10',
                  timestamp: '2022-01-01T00:00:00.000Z',
                  quality: 'VALID',
                  writeValue: '0',
                },
              },
            },
          }),
        },
      ],
    });
  });

});

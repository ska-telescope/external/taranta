import { extractDevicesFromDashboard } from './ExtractDevicesFromDashboard'

describe("test util extractDevicesFromDashboard function", () => {

    const variables = [
        {
            "_id": "62b43884a5d09900122779f4",
            "name": "das",
            "class": "Starter",
            "device": "tango/admin/6dbbdbf9b28d"
        },
        {
            "_id": "62b42eedc7c72100189ce7e9",
            "name": "MyVar2",
            "class": "WebjiveTestDevice",
            "device": "test/webjive-testdevice/1"
        }
    ];

    const widgets = [
        {
            "id": "14",
            "x": 79,
            "y": 29,
            "canvas": "0",
            "width": 26,
            "height": 4,
            "type": "DEVICE_STATUS",
            "inputs": {
                "device": "sys/tg_test/1",
                "state": {
                    "device": null,
                    "attribute": null
                },
                "showDeviceName": true,
                "showStateString": true,
                "showStateLED": true,
                "LEDSize": 1,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "textSize": 1
            },
            "valid": true,
            "order": 8
        },
        {
            "id": "13",
            "x": 107,
            "y": 13,
            "canvas": "0",
            "width": 40,
            "height": 6,
            "type": "COMMAND",
            "inputs": {
                "command": {
                    "device": "sys/tg_test/1",
                    "command": "CrashFromOmniThread",
                    "acceptedType": "DevVoid",
                    "intypedesc": "Uninitialised",
                    "outtypedesc": "Uninitialised",
                    "outtype": "DevVoid",
                    "tag": "0"
                },
                "commandArgs": [],
                "showDevice": true,
                "showCommand": true,
                "requireConfirmation": true,
                "displayOutput": true,
                "placeholder": "intype",
                "OutputMaxSize": 20,
                "timeDisplayOutput": 3000,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "font": "Helvetica"
            },
            "valid": true,
            "order": 7
        },
        {
            "id": "12",
            "x": 109,
            "y": 6,
            "canvas": "0",
            "width": 24,
            "height": 4,
            "type": "Command_Switch",
            "inputs": {
                "onCommand": {
                    "device": "sys/tg_test/1",
                    "command": "DevBoolean",
                    "acceptedType": "DevBoolean",
                    "intypedesc": "Any boolean value",
                    "outtypedesc": "Echo of the argin value",
                    "outtype": "DevBoolean",
                    "tag": "0"
                },
                "offCommand": {
                    "device": "sys/access_control/1",
                    "command": "AddDeviceForUser",
                    "acceptedType": "DevVarStringArray",
                    "intypedesc": "user name, device adn value",
                    "outtypedesc": "Uninitialised",
                    "outtype": "DevVoid",
                    "tag": "0"
                },
                "showDevice": true,
                "showCommand": true,
                "displayOutput": true,
                "timeDisplayOutput": 3000
            },
            "valid": true,
            "order": 6
        },
        {
            "id": "11",
            "x": 45,
            "y": 25,
            "canvas": "0",
            "width": 27,
            "height": 19,
            "type": "SPECTRUM_2D",
            "inputs": {
                "attributeX": {
                    "device": "sys/tg_test/1",
                    "attribute": "double_spectrum",
                    "label": "double_spectrum"
                },
                "attributes": [
                    {
                        "attribute": {
                            "device": "test/webjive-testdevice/1",
                            "attribute": "spectrum_att",
                            "label": "spectrum_att"
                        }
                    },
                    {
                        "attribute": {
                            "device": "MyVar2",
                            "attribute": "spectrum_att",
                            "label": "spectrum_att"
                        }
                    }
                ],
                "showAttribute": "Label",
                "showTitle": true
            },
            "valid": true,
            "order": 5
        },
        {
            "id": "10",
            "x": 114,
            "y": 23,
            "canvas": "0",
            "width": 60,
            "height": 40,
            "type": "ATTRIBUTEHEATMAP",
            "inputs": {
                "attribute": {
                    "device": "sys/tg_test/1",
                    "attribute": "boolean_image",
                    "label": "boolean_image"
                },
                "selectAxisAttribute": false,
                "xAxis": {
                    "device": "test/webjivetestdevice/1",
                    "attribute": "spectrum_att",
                    "label": "spectrum_att"
                },
                "yAxis": {
                    "device": "test/webjivetestdevice/1",
                    "attribute": "spectrum_att",
                    "label": "spectrum_att"
                },
                "showTitle": true,
                "showAttribute": "Label",
                "fixedScale": true,
                "maxValue": 30,
                "minValue": 1
            },
            "valid": true,
            "order": 4
        },
        {
            "id": "9",
            "x": 3,
            "y": 13,
            "canvas": "0",
            "width": 29,
            "height": 10,
            "type": "MACROBUTTON",
            "inputs": {
                "door": "sys/database/2",
                "macroserver": "dserver/starter/6dbbdbf9b28d",
                "pool": "sys/access_control/1",
                "state": {
                    "device": null,
                    "attribute": null
                },
                "macrolist": {
                    "device": null,
                    "attribute": null
                },
                "motorlist": {
                    "device": null,
                    "attribute": null
                }
            },
            "valid": true,
            "order": 3
        },
        {
            "id": "15",
            "x": 58.3,
            "y": 47.8,
            "canvas": "0",
            "width": 43.4,
            "height": 12.4,
            "type": "ATTRIBUTE_SCATTER",
            "inputs": {
                "showAttribute": "Label",
                "independent": {
                    "device": "test/webjive-testdevice/1",
                    "attribute": "randomattr",
                    "label": "RandomAttr"
                },
                "dependent": {
                    "device": "sys/tg_test/1",
                    "attribute": "double_scalar_rww",
                    "label": "double_scalar_rww"
                }
            },
            "valid": true,
            "order": 3
        },
        {
            "id": "8",
            "x": 2,
            "y": 25,
            "canvas": "0",
            "width": 35,
            "height": 22,
            "type": "SPECTRUM_2D",
            "inputs": {
                "attributeX": {
                    "device": "sys/database/2",
                    "attribute": "timing_average",
                    "label": "Timing_average"
                },
                "attributes": [],
                "showAttribute": "Label",
                "showTitle": true
            },
            "valid": true,
            "order": 2
        },
        {
            "id": "4",
            "x": 50,
            "y": 3,
            "canvas": "0",
            "width": 45,
            "height": 23,
            "type": "BOX",
            "inputs": {
                "title": "",
                "bigWidget": 4,
                "smallWidget": 1,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "borderColor": "",
                "borderWidth": 0,
                "borderStyle": "solid",
                "textSize": 1,
                "fontFamily": "Helvetica",
                "layout": "vertical",
                "alignment": "Center",
                "padding": 0,
                "customCss": ""
            },
            "valid": true,
            "order": 1,
            "hover": [],
            "innerWidgets": [
                {
                    "id": "3",
                    "x": 50.4,
                    "y": 5.24,
                    "canvas": "0",
                    "width": 44.2,
                    "height": 4.1,
                    "type": "ATTRIBUTE_DISPLAY",
                    "inputs": {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "status",
                            "label": "Status"
                        },
                        "precision": 2,
                        "showDevice": false,
                        "showAttribute": "Label",
                        "scientificNotation": false,
                        "showEnumLabels": false,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "font": "Helvetica"
                    },
                    "valid": true,
                    "order": 1
                },
                {
                    "id": "6",
                    "x": 50.4,
                    "y": 9.34,
                    "canvas": "0",
                    "width": 44.2,
                    "height": 16.4,
                    "type": "BOX",
                    "inputs": {
                        "title": "",
                        "bigWidget": 4,
                        "smallWidget": 1,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "borderColor": "",
                        "borderWidth": 0,
                        "borderStyle": "solid",
                        "textSize": 1,
                        "fontFamily": "Helvetica",
                        "layout": "vertical",
                        "alignment": "Center",
                        "padding": 0,
                        "customCss": ""
                    },
                    "valid": true,
                    "innerWidgets": [
                        {
                            "id": "5",
                            "x": 50.8,
                            "y": 11.052,
                            "canvas": "0",
                            "width": 43.4,
                            "height": 2.9,
                            "type": "ATTRIBUTE_DISPLAY",
                            "inputs": {
                                "attribute": {
                                    "device": "test/webjive-testdevice/1",
                                    "attribute": "cspobsstate",
                                    "label": "CspObsState"
                                },
                                "precision": 2,
                                "showDevice": false,
                                "showAttribute": "Label",
                                "scientificNotation": false,
                                "showEnumLabels": false,
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "font": "Helvetica"
                            },
                            "valid": true,
                            "order": 1
                        },
                        {
                            "id": "7",
                            "x": 50.8,
                            "y": 13.952,
                            "canvas": "0",
                            "width": 43.4,
                            "height": 11.6,
                            "type": "ATTRIBUTE_PLOT",
                            "inputs": {
                                "timeWindow": 120,
                                "showZeroLine": true,
                                "logarithmic": false,
                                "attributes": [
                                    {
                                        "attribute": {
                                            "device": "test/webjive-testdevice/1",
                                            "attribute": "randomattr",
                                            "label": "RandomAttr"
                                        },
                                        "showAttribute": "Label",
                                        "yAxis": "left",
                                        "lineColor": "#000000"
                                    }
                                ],
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff"
                            },
                            "valid": true,
                            "order": 2
                        }
                    ],
                    "order": 2,
                    "hover": []
                }
            ]
        },
        {
            "_id": "62c4147708b320001816460c",
            "id": "2",
            "x": 2.3000000000000007,
            "y": 2.8000000000000007,
            "canvas": "0",
            "width": 43.4,
            "height": 8.4,
            "type": "ELASTICSEARCH_LOG_VIEWER",
            "inputs": {
                "elasticsearchUrl": ""
            },
            "order": 0,
            "valid": true
        }
    ];

    it("test if it returns the same object it the comparison argument is different from enumlabels or attributename ", () => {

        expect(extractDevicesFromDashboard(variables, widgets)).toEqual(["sys/tg_test/1",
        "sys/access_control/1",
        "test/webjive-testdevice/1",
        "test/webjivetestdevice/1",
        "sys/database/2",
        "dserver/starter/6dbbdbf9b28d",
       ]);
    })


})
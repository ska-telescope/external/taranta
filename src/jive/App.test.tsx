// App.test.tsx
import React from 'react';
import { render, screen } from '@testing-library/react';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import App from './App';
import { FETCH_DEVICE_SERVER_NAMES } from '../shared/state/actions/actionTypes';
import { getTangoDB } from '../dashboard/dashboardRepo';

// Mock child components
jest.mock('../shared/user/components/UserAware', () => ({ children }: any) => <div>{children}</div>);
jest.mock('./components/Layout/Layout', () => () => <div>Layout Component</div>);
jest.mock('./components/InfoPage/InfoPage', () => () => <div>InfoPage Component</div>);

// Mock getTangoDB
jest.mock('../dashboard/dashboardRepo', () => ({
  getTangoDB: jest.fn(),
}));

const mockStore = configureStore([]);
let store: MockStoreEnhanced<unknown, {}>;
const mockDispatch = jest.fn();

beforeEach(() => {
  store = mockStore({
    deviceList: {
      nameList: [],
      filter: '',
    },
    loadingStatus: {
      loadingNames: false,
      loadingDevice: false,
      loadingOutput: {},
    },
    // Add other necessary initial state slices here
  });

  store.dispatch = mockDispatch;

  // Mock getTangoDB to return a test value
  (getTangoDB as jest.Mock).mockReturnValue('testDB');

  // Mock window config
  (window as any).config = {
    basename: '/app',
  };
});

afterEach(() => {
  jest.clearAllMocks();
});

const renderWithProviders = (ui: React.ReactElement, history: any) => {
  return render(
    <Provider store={store}>
      <Router history={history}>
        {ui}
      </Router>
    </Provider>
  );
};

describe('App Component', () => {
  it('dispatches FETCH_DEVICE_NAMES action on mount', () => {
    const history = createMemoryHistory();
    renderWithProviders(<App />, history);

    expect(mockDispatch).toHaveBeenCalledWith({
      type: FETCH_DEVICE_SERVER_NAMES,
      tangoDB: 'testDB',
    });
  });

  it('renders InfoPage component at root path "/"', () => {
    const history = createMemoryHistory();
    history.push('/testdb'); 

    renderWithProviders(<App />, history);

    expect(screen.getByText('InfoPage Component')).not.toBeNull();
  });
  
});

import React from 'react';
import { configure, mount } from 'enzyme';
import ScrollIntoViewIfNeeded from './ScrollIntoView';
import Adapter from "@cfaester/enzyme-adapter-react-18";
import '@testing-library/jest-dom';

configure({ adapter: new Adapter() });

describe('ScrollIntoViewIfNeeded component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <ScrollIntoViewIfNeeded active={true} isSelected={true}>
        <div>Hello World</div>
      </ScrollIntoViewIfNeeded>
    );
  });

  it('should render the children elements', () => {
    expect(wrapper.html()).toContain('Hello World');
  });

});
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { WEBSOCKET } from "../../../../shared/state/actions/actionTypes";

import "./ServerInfo.css";

const ServerTable = ({ server }) => (
  <table>
    <tbody>
      <tr>
        <th>Server</th>
        <td>{server.id}</td>
      </tr>
      <tr>
        <th>Host</th>
        <td>{server.host}</td>
      </tr>
    </tbody>
  </table>
);

const ServerInfo = ({ device }) => {
  const { connected, server } = device;

  const dispatch = useDispatch();

    useEffect(() => {
        const subUnsubToState = (eventType) => {
            dispatch({
                type: eventType,
                payload: {
                    devices: [device.name+'/state'],
                },
            });
        }
        subUnsubToState(WEBSOCKET.WS_SUBSCRIBE);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [device.name]);

  const inner =
    connected === false ? (
      "Couldn't connect to the device."
    ) : server == null ? (
      "No server information available."
    ) : (
      <ServerTable server={server} />
    );

  return <div className="ServerInfo">{inner}</div>;
};

ServerInfo.propTypes = {
  connected: PropTypes.bool,
  server: PropTypes.shape({
    id: PropTypes.string,
    host: PropTypes.string
  })
};

export default ServerInfo;

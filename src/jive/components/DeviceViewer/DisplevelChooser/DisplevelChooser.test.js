import React from "react";
import { configure, shallow } from "enzyme";
import DisplevelChooser from "./DisplevelChooser";
import Adapter from "@cfaester/enzyme-adapter-react-18";

configure({ adapter: new Adapter() });

const setup = (overrideProps = {}) => {
  const props = {
    device: {
      attributes: [{ displevel: "one" }, { displevel: "two" }],
      commands: [{ displevel: "three" }, { displevel: "four" }]
    },
    disabledDisplevels: ["two"],
    onChange: jest.fn(),
    ...overrideProps
  };
  const wrapper = shallow(<DisplevelChooser {...props} />);
  return { wrapper, props };
};

describe("DisplevelChooser", () => {
  it("renders without crashing", () => {
    const { wrapper } = setup();
    expect(wrapper).toBeTruthy();
  });

  it("displays the correct number of checkboxes", () => {
    const { wrapper } = setup();
    expect(wrapper.find("input[type='checkbox']").length).toBe(4);
  });

  it("passes the correct props to the checkboxes", () => {
    const { wrapper } = setup();
    const checkboxes = wrapper.find("input[type='checkbox']");

    expect(checkboxes.at(0).props().id).toBe("displevel_one");
    expect(checkboxes.at(0).props().checked).toBe(true);
    expect(checkboxes.at(1).props().id).toBe("displevel_two");
    expect(checkboxes.at(1).props().checked).toBe(false);
    expect(checkboxes.at(2).props().id).toBe("displevel_three");
    expect(checkboxes.at(2).props().checked).toBe(true);
    expect(checkboxes.at(3).props().id).toBe("displevel_four");
    expect(checkboxes.at(3).props().checked).toBe(true);
  });

  
  it("calls onChange when a checkbox is clicked", () => {
    const { wrapper, props } = setup();
    const checkboxes = wrapper.find("input[type='checkbox']");

    checkboxes.at(0).simulate("change",{target: {checked: false}});
    expect(props.onChange).toHaveBeenCalledWith("one", false);

    checkboxes.at(1).simulate("change",{target: {checked: true}});
    expect(props.onChange).toHaveBeenCalledWith("two", true);
  });
  
}); 

import React from "react";
import { Provider } from "react-redux";
import { ImageDisplay } from "./ImageDisplay";
import { configure, mount } from 'enzyme';
import Adapter from "@cfaester/enzyme-adapter-react-18";

configure({ adapter: new Adapter() });

const store = {
  getState: () => ({
    messages: {
      device1: {
        attributes: 
        {
            attribute1: {
                quality: [
                    "ATTR_VALID"
                ],
                timestamp: [
                    1675353614.375299
                ],
                values: [[[1,2,3],[1,2,3],[1,2,3]]]
            }
        }
    }
}
  }),
  subscribe: () => {},
  dispatch: () => {}
};

describe("ImageDisplay", () => {
  it("renders without crashing", () => {
    const props = {
      device: "device1",
      attribute: "attribute1"
    };
    const canvasElement = document.createElement("canvas");
    const canvasGetContextSpy = jest.spyOn(canvasElement, "getContext");
    canvasGetContextSpy.mockImplementation(() => ({
      createImageData: jest.fn(),
      putImageData: jest.fn()
    }));
    const wrapper = mount(
      <Provider store={store}>
        <ImageDisplay {...props} />
      </Provider>
    );

    const canvasRef = wrapper.find("canvas").get(0);
    expect(canvasRef).toBeTruthy();
  });
});

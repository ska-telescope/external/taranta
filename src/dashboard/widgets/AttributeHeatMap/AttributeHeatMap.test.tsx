import React, { ReactElement } from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import AttributeHeatMap from "./AttributeHeatMap";
import { AttributeInput } from "../../types";

interface Input {
  attribute: AttributeInput,
  xAxis: AttributeInput,
  yAxis: AttributeInput,
  showTitle: boolean,
  showTangoDB: boolean,
  selectAxisAttribute: boolean,
  showAttribute: string,
  fixedScale: boolean,
  maxValue: number,
  minValue: number
};

configure({ adapter: new Adapter() });

describe("AttributeHeatMap Widget Tests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  let xAxisInput: AttributeInput, yAxisInput: AttributeInput;
  var writeArray: any = [];
  var date = new Date();
  let element: ReactElement;

  function getReactElement(mode: string, myInput: Input): ReactElement {
    return React.createElement(AttributeHeatMap.component, {
      mode: mode,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
  }

  beforeEach(() => {
    xAxisInput = {
      device: "sys/tg_test/x",
      attribute: "x-axis",
      label: "x-axis",
      isNumeric: true,
      write: writeArray,
      writeValue: [0],
      value: [1, 2, 3],
      timestamp: date.getTime(),
      history: [],
      dataFormat: "spectrum",
      dataType: "DevDouble",
      enumlabels: [],
      unit: "",
      quality: "VALID"
    };

    yAxisInput = {
      device: "sys/tg_test/y",
      attribute: "y-axis",
      label: "y-axis",
      isNumeric: true,
      write: writeArray,
      writeValue: [0],
      value: [1, 2, 3],
      timestamp: date.getTime(),
      history: [],
      dataFormat: "spectrum",
      dataType: "DevDouble",
      enumlabels: [],
      unit: "",
      quality: "VALID"
    };

    myAttributeInput = {
      device: "sys/tg_test/Input",
      attribute: "Image",
      label: "Image",
      isNumeric: true,
      write: writeArray,
      writeValue: [0],
      value: [
        [30, 60, 1],
        [20, 1, 60],
        [1, 20, 30],
      ],
      timestamp: date.getTime(),
      history: [],
      dataFormat: "spectrum",
      dataType: "DevDouble",
      enumlabels: [],
      unit: "",
      quality: "VALID"
    }

    myInput = {
      attribute: myAttributeInput,
      xAxis: xAxisInput,
      yAxis: yAxisInput,
      showTitle: true,
      showTangoDB: false,
      selectAxisAttribute: false,
      showAttribute: "Label",
      fixedScale: true,
      maxValue: 30,
      minValue: 1,
    }
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("renders with correct title", () => {
    element = getReactElement("run", myInput);
    expect(shallow(element).instance()['getTitle']()).toEqual("sys/tg_test/Input/Image");
    myInput.showTitle = false;

    element = getReactElement("run", myInput);
    expect(shallow(element).instance()['getTitle']()).toEqual(null);
  });

  it("sets display to the right value when showAttribute = 'Label'", () => {
    myInput.showAttribute = "Label";
    myInput.attribute.label = "HeatMap Label";
    element = getReactElement("run", myInput);
    let expectedData = "sys/tg_test/Input/HeatMap Label";
    expect(shallow(element).instance()['getLayout']().title).toEqual(expectedData);

    myInput.attribute.label = "";
    element = getReactElement("run", myInput);
    expectedData = "sys/tg_test/Input/attributeLabel";
    expect(shallow(element).instance()['getLayout']().title).toEqual(expectedData);
  });

  it("sets display to the right value when showAttribute = 'Name'", () => {
    myInput.showAttribute = "Name";
    myInput.attribute.attribute = "HeatMap Name";
    element = getReactElement("run", myInput);
    let expectedData = "sys/tg_test/Input/HeatMap Name";
    expect(shallow(element).instance()['getLayout']().title).toEqual(expectedData);

    myInput.attribute.attribute = "";
    element = getReactElement("run", myInput);
    expectedData = "sys/tg_test/Input/attributeName";
    expect(shallow(element).instance()['getLayout']().title).toEqual(expectedData);
  });

  it("checks componentDidMount and setState have been called", () => {
    let time = date.getTime();
    process.env.REACT_APP_REFRESHING_RATE = "123"
    element = getReactElement("run", myInput);
    const instance = shallow(element).instance();
    jest.spyOn(instance, 'componentDidMount').mockImplementation(() => { instance.setState({ time: time }); });
    jest.spyOn(instance, 'setState');
    instance.componentDidMount?.();
    expect(instance.componentDidMount).toHaveBeenCalledTimes(1);
    expect(instance.setState).toHaveBeenCalledTimes(1);
    expect(instance.setState).toHaveBeenCalledWith({ "time": time });
  });

  it("checks componentWillUnmount clearInterval have been called", () => {
    element = getReactElement("run", myInput);
    const instance = shallow(element).instance();
    instance['interval'] = setInterval(() => { }, 1000);
    jest.spyOn(window, 'clearInterval');
    instance.componentWillUnmount?.();
    expect(clearInterval).toHaveBeenCalledWith(instance['interval']);
  });

  it("checks shouldComponentUpdate returns the correct update value", () => {
    let time = date.getTime();
    element = getReactElement("run", myInput);
    const instance = shallow(element).instance();
    instance['interval'] = true;
    instance['interval'] = true;
    instance.state['time'] = time;
    const nextState = { time: time + 1000 };
    const nextProps = {};
    expect(instance.shouldComponentUpdate?.(nextProps, nextState,null)).toEqual(true);
    expect(instance['rendered']).toBe(false);

    instance['rendered'] = true;
    nextState.time = time;
    expect(instance.shouldComponentUpdate?.(nextProps, nextState,null)).toEqual(false);

    instance['interval'] = false;
    nextState.time = time + 1000;
    expect(instance.shouldComponentUpdate?.(nextProps, nextState,null)).toEqual(true);
    expect(instance['rendered']).toBe(false);

    instance['rendered'] = false;
    expect(instance.shouldComponentUpdate?.(nextProps, nextState,null)).toEqual(false);
  });
});

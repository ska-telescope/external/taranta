import React from "react";

import {
  AttributeInput
} from "../../types";

import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import BooleanDisplay from "./BooleanDisplay";

jest.mock("./BooleanValues", () => () => (
  <div>Mock BooleanValues</div>
));

type Input = {
  showDevice: boolean;
  showTangoDB: boolean;
  alignSwitchRight: boolean;
  attribute: AttributeInput;
  showAttribute: string;
  widgetCSS: string;
  OnCSS: string;
  OffCSS: string;
};

configure({ adapter: new Adapter() });

describe("Boolean Display", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();


  it("set checked if display device name", () => {
    myAttributeInput = {
      device: "tangodb1://sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
      label: "BOOLEAN_SCALAR"
    };

    myInput = {
      showDevice: true,
      showTangoDB: false,
      alignSwitchRight: true,
      attribute: myAttributeInput,
      showAttribute: 'Name',
      widgetCSS: '',
      OnCSS: '',
      OffCSS: ''
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("sys/tg_test/1");
  });

  it("set checked if display tangodb name name", () => {
    myAttributeInput = {
      device: "tangodb1://sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
      label: "BOOLEAN_SCALAR"
    };

    myInput = {
      showDevice: true,
      showTangoDB: true,
      attribute: myAttributeInput,
      alignSwitchRight: true,
      showAttribute: 'Name',
      widgetCSS: '',
      OnCSS: '',
      OffCSS: ''
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("tangodb1://sys/tg_test/1");
  });

  it("set checked if hide device name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      history: [],
      dataType: "DevBoolean",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: true,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
      label: "BOOLEAN_SCALAR"
    };

    myInput = {
      showDevice: false,
      showTangoDB: false,
      alignSwitchRight: true,
      attribute: myAttributeInput,
      showAttribute: 'Name',
      widgetCSS: '',
      OnCSS: '',
      OffCSS: ''
    };

    const element = React.createElement(BooleanDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1");
  });

});

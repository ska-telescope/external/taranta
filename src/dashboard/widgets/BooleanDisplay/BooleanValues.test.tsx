import React, { CSSProperties } from "react";
import { render, fireEvent, act } from "@testing-library/react";
import { useSelector as useSelectorMock, useDispatch } from "react-redux";
import BooleanValues from "./BooleanValues";
import {
  getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("../../../dashboard/dashboardRepo", () => ({
  getTangoDB: jest.fn(),
}));

const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

const styleCheckbox = { paddingLeft: "0em" } as CSSProperties;

describe("BooleanValues", () => {
  const dispatch = jest.fn();

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {
          "sys/tg_test/1": {
            attributes: {
              boolean_scalar: {
                values: [true, true, true],
                quality: ["ATTR_VALID", "ATTR_VALID", "ATTR_VALID"],
                timestamp: [
                  1689077614.017916,
                  1689077614.061947,
                  1689077674.123604,
                ],
              },
            },
          },
        },
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockReturnValue(true);
    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    (useDispatch as jest.Mock).mockReturnValue(dispatch);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render the boolean value from store", () => {
    const { container } = render(
      <BooleanValues
        attributeName="boolan_scalar"
        deviceName="/sys/tg_test/1"
        mode="run"
        dataType="DevBoolean"
        styleCheckbox={styleCheckbox}
        OnCSS={""}
        OffCSS={""}
      />
    );

    expect(container.innerHTML).toContain('value="true"');
    expect(container.innerHTML).toContain("checked");
  });

  it("should not render no boolean attributes", () => {
    const { container } = render(
      <BooleanValues
        attributeName="boolan_scalar"
        deviceName="/sys/tg_test/1"
        mode="run"
        dataType="DevString"
        styleCheckbox={styleCheckbox}
        OnCSS={""}
        OffCSS={""}
      />
    );

    expect(container.innerHTML).toContain(
      "Selected attribute is different than boolean"
    );
  });

  it("should change value", () => {
    const element = render(
      <BooleanValues
        attributeName="boolan_scalar"
        deviceName="/sys/tg_test/1"
        mode="run"
        dataType="DevBoolean"
        styleCheckbox={styleCheckbox}
        OnCSS={""}
        OffCSS={""}
      />
    );

    let setAttribute = element.getByTitle("boolean_display");
    fireEvent.click(setAttribute);
    expect(dispatch).toHaveBeenCalled();
  });

  it("disable and enable the button after 3 seconds", () => {
    jest.useFakeTimers();

    const element = render(
      <BooleanValues
        attributeName="boolan_scalar"
        deviceName="/sys/tg_test/1"
        mode="run"
        dataType="DevBoolean"
        styleCheckbox={styleCheckbox}
        OnCSS={""}
        OffCSS={""}
      />
    );
    let checkbox = element.getByTitle("boolean_display");

    fireEvent.click(checkbox);
    act(() => {
      jest.advanceTimersByTime(1000);
    });

    expect(element.container.innerHTML).toContain("disabled");

    act(() => {
      jest.advanceTimersByTime(5000);
    });

    expect(element.container.innerHTML).not.toContain("disabled");

  });
});

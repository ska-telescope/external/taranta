import React from 'react';
import configureStore from '../../../shared/state/store/configureStore';
import { Provider } from 'react-redux';
import {
  render,
  screen,
  fireEvent,
  waitFor,
  cleanup,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import AttributeWriterDropDown from './AttributeWriterDropDown';

jest.mock('./AttributeWriterButton', () => () => (
  <div>Mock AttributeWriterButton</div>
));

describe('AttributeWriterDropdown', () => {
  const store = configureStore();
  const renderComponent = (mode, inputs) => {
    return render(
      <Provider store={store}>
        <AttributeWriterDropDown.component
          mode={mode}
          t0={1}
          actualWidth={100}
          actualHeight={100}
          inputs={inputs}
          id={42}
        />
      </Provider>
    );
  };

  const date = new Date();
  const timestamp = date.getTime();
  const initialAttributeInput = {
    device: '',
    attribute: '',
    label: '',
    quality: '',
    history: [],
    dataType: '',
    dataFormat: '',
    isNumeric: true,
    unit: '',
    enumlabels: [],
    write: () => {},
    value: '',
    writeValue: '',
    timestamp: timestamp,
  };

  const inputs = {
    attribute: initialAttributeInput,
    dropdownTitle: '',
    submitButtonTitle: '',
    writeValues: [],
    showDevice: true,
    showAttribute: 'Label',
    textColor: '',
    backgroundColor: '',
    size: 1,
    font: 'Helvetica',
    dropdownButtonCss: '',
    submitButtonCss: '',
  };

  it('renders an empty widget in edit mode without crashing', () => {
    renderComponent('edit', inputs);
    expect(screen.getByText('Mock AttributeWriterButton')).toBeInTheDocument();
    expect(screen.getByText('Dropdown')).toBeInTheDocument();
  });

  it('renders an empty widget in run mode without crashing', () => {
    renderComponent('run', inputs);
    expect(screen.getByText('Mock AttributeWriterButton')).toBeInTheDocument();
    expect(screen.getByText('Dropdown')).toBeInTheDocument();
  });

  it('renders Dropdown with empty input on write values', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'string',
      label: 'test_string',
      dataType: 'DevString',
      isNumeric: false,
    };

    const testInput = {
      ...inputs,
      writeValues: [
        { title: '', value: '' },
        { title: '', value: '' },
      ],
      attribute: testAttributeInput,
    };

    renderComponent('run', testInput);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);

    waitFor(() => {
      const dropdownItems = screen.getAllByText('Empty string');
      expect(dropdownItems.length).toBeGreaterThanOrEqual(2);
    });
  });
  it('renders Dropdown with data type boolean of write values', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'boolean',
      label: 'test_boolean',
      dataType: 'DevBoolean',
      isNumeric: false,
    };

    const testInput = {
      ...inputs,
      showTangoDB: true,
      writeValues: [
        { title: 'On', value: 'true' },
        { title: 'Off', value: 'false' },
      ],
      attribute: testAttributeInput,
      showAttribute: 'Name',
    };

    renderComponent('run', testInput);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);

    waitFor(() => {
      expect(screen.getByText('On')).toBeInTheDocument();
      expect(screen.getByText('true')).toBeInTheDocument();
      expect(screen.getByText('Off')).toBeInTheDocument();
      expect(screen.getByText('false')).toBeInTheDocument();
    });
  });
  it('renders Dropdown with data type other (not number, string or boolean) of write values', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'other',
      label: 'test_other_data_type',
      dataType: 'DevState',
      isNumeric: false,
    };

    const testInput = {
      ...inputs,
      writeValues: [
        { title: 'On', value: 'true' },
        { title: 'Off', value: 'false' },
      ],
      attribute: testAttributeInput,
      showAttribute: 'Name',
    };

    renderComponent('run', testInput);

    waitFor(() => {
      expect(screen.getByText('DevState is not supported')).toBeInTheDocument();
    });
  });

  it('renders Dropdown with wrong type for write value', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'number',
      label: 'test_number',
      isNumeric: true,
    };

    const testInput = {
      ...inputs,
      writeValues: [{ title: 'highest', value: 'highest' }],
      attribute: testAttributeInput,
      showDevice: false,
      showAttribute: 'Name',
    };

    renderComponent('run', testInput);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);

    waitFor(() => {
      expect(screen.getByText('Invalid type for value')).toBeInTheDocument();
    });
  });

  it('test dropdownTitle change', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'number',
      label: 'test_number',
      isNumeric: true,
    };

    const testInput = {
      ...inputs,
      writeValues: [
        { title: 'highest', value: '10' },
        { title: 'lowest', value: '1' },
      ],
      attribute: testAttributeInput,
    };

    renderComponent('run', testInput);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    userEvent.click(dropdownButton);

    const firstOption = await screen.findByText('highest');
    userEvent.click(firstOption);

    waitFor(() => {
      expect(dropdownButton).toHaveTextContent('highest');
    });
    userEvent.click(dropdownButton);

    const resetOption = await screen.findByText('Reset selection');
    userEvent.click(resetOption);

    waitFor(() => {
      expect(dropdownButton).toHaveTextContent('Dropdown');
    });
  });

  it('renders Dropdown with NO write value / with write values ', async () => {
    let testInputNoWriteValue = {
      ...inputs,
      attribute: {
        ...initialAttributeInput,
        device: 'tangotest://sys/tg_test/1',
        attribute: 'numeric',
        label: 'test_numeric',
      },
      writeValues: [],
    };

    renderComponent('run', testInputNoWriteValue);

    fireEvent.click(screen.getByRole('button', { name: /Dropdown/i }));

    expect(
      await screen.findByText('No write value available')
    ).toBeInTheDocument();

    cleanup();

    const testInputWithWriteValues = {
      ...inputs,
      attribute: {
        ...initialAttributeInput,
        device: 'tangotest://sys/tg_test/1',
        attribute: 'numeric',
        label: 'test_numeric',
      },
      writeValues: [
        { title: 'highest', value: '10' },
        { title: 'lowest', value: '1' },
      ],
    };

    renderComponent('run', testInputWithWriteValues);

    fireEvent.click(screen.getByRole('button', { name: /Dropdown/i }));

    expect(screen.getByText('highest')).toBeInTheDocument();
    expect(screen.getByText('lowest')).toBeInTheDocument();
  });

  it('renders Dropdown with write values from device', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'numeric',
      label: 'test_numeric',
    };

    const testInputWriteValue = {
      ...inputs,
      writeValuesSpectrum: { value: ['option1', 'option2'] },
      attribute: testAttributeInput,
    };

    renderComponent('run', testInputWriteValue);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);

    const option1 = await screen.findByTitle('write value: option1');
    expect(option1).toBeInTheDocument();
    expect(option1).toHaveAttribute('disabled');

    const option2 = await screen.findByTitle('write value: option2');
    expect(option2).toBeInTheDocument();
    expect(option2).toHaveAttribute('disabled');
  });

  it('renders Dropdown with write values from device and provided by user', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'numeric',
      label: 'test_numeric',
    };

    const testInputWriteValue = {
      ...inputs,
      writeValuesSpectrum: { value: ['option1', 'option2'] },
      writeValues: [
        { title: 'highest', value: '10' },
        { title: 'lowest', value: '1' },
      ],
      attribute: testAttributeInput,
    };

    renderComponent('run', testInputWriteValue);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);
    const highestOption = await screen.findByText('highest');
    expect(highestOption).toBeInTheDocument();

    expect(highestOption).toHaveAttribute('title', 'write value: 10 ');
    const lowestOption = await screen.findByText('lowest');
    expect(lowestOption).toBeInTheDocument();

    expect(lowestOption).toHaveAttribute('title', 'write value: 1 ');
  });

  it('renders Dropdown with data type boolean of write values', async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: null,
      label: 'test_numeric',
    };

    let testInputWriteValue = {
      ...inputs,
      writeValuesSpectrum: { value: ['option1', 'option2'] },
      writeValues: [
        { title: 'highest', value: '10' },
        { title: 'lowest', value: '1' },
      ],
      attribute: testAttributeInput,
      showAttribute: 'Name',
    };

    renderComponent('run', testInputWriteValue);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);
    cleanup();

    const testAttributeInput1 = {
      ...initialAttributeInput,
      device: 'tangotest://sys/tg_test/1',
      attribute: 'numeric',
      label: 'test_numeric',
    };

    const testInputWriteValue1 = {
      ...inputs,
      writeValuesSpectrum: { value: ['option1', 'option2'] },
      writeValues: [{ value: '10a' }, { title: 'lowest', value: '1' }],
      attribute: testAttributeInput1,
      showAttribute: 'Name',
    };

    renderComponent('run', testInputWriteValue1);

    fireEvent.click(screen.getByRole('button', { name: /Dropdown/i }));
  });

  it('renders Dropdown with data type boolean of write values', () => {
    const testInputWriteValue = {
      ...inputs,
      writeValuesSpectrum: { value: ['option1', 'option2'] },
      writeValues: [
        { title: 'highest', value: '10' },
        { title: 'lowest', value: '1' },
      ],
      attribute: {
        ...initialAttributeInput,
        device: 'tangotest://sys/tg_test/1',
        attribute: 'numeric',
        label: 'test_numeric',
      },
    };

    renderComponent('run', testInputWriteValue);

    const dropdownButton = screen.getByRole('button', { name: /Dropdown/i });
    fireEvent.click(dropdownButton);

    expect(screen.getByTitle('write value: option1')).toBeInTheDocument();
    expect(screen.getByTitle('write value: option2')).toBeInTheDocument();
    expect(screen.getByText('highest')).toBeInTheDocument();
    expect(screen.getByText('lowest')).toBeInTheDocument();
  });
});

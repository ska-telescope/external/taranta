import React, { Suspense, useRef, useState } from "react";
import { sortedIndex } from "lodash";

// In order to avoid importing the entire plotly.js library. Note that this mutates the global PlotlyCore object.
import PlotlyCore from "plotly.js/lib/core";
import PlotlyScatter from "plotly.js/lib/scatter";
import createPlotlyComponent from "react-plotly.js/factory";
import { showHideTangoDBName } from "../../DBHelper";
import { HISTORY_LIMIT } from "../../components/RunCanvas/RunCanvas";

PlotlyCore.register([PlotlyScatter]);
export const Plotly = createPlotlyComponent(PlotlyCore);

export interface PlotParams {
  height: number;
  width: number;
  staticMode?: boolean;
  timeWindow: number;
  showZeroLine?: boolean;
  logarithmic?: boolean;
  showTangoDB: boolean;
  textColor: string;
  backgroundColor: string;
}

export interface Trace {
  x?: number[];
  y?: number[];
  fullName: string;
  axisLocation: string; //"left" | "right";
  lineColor: string;
}

interface PlotProps {
  traces: Trace[];
  params: PlotParams;
  mode: string;
  showTangoDB: boolean;
}

enum Order {
  ASCENDING = "ASC",
  DESCENDING = "DESC",
}

export function sortedSearch(
  array: number[],
  target: number,
  order: Order = Order.ASCENDING
): number {

  // Get effective index according to specified order
  const getIndex = (i: number) =>
    order === Order.DESCENDING ? array.length - 1 - i : i;
  const isFound = (a: number, b: number) =>
    order === Order.ASCENDING ? a >= b : a <= b;

  for (let i = 0; i < array.length; i++) {
    const index = getIndex(i);
    if (isFound(array[index], target)) {
      return index;
    }
  }

  // The target is not found.
  return array.length - 1;
}

export class TraceHistory {
  public x: number[];
  public y: number[];
  public window: number;

  constructor({
    x = [],
    y = [],
    window,
  }: {
    x?: number[];
    y?: number[];
    window: number;
  }) {
    this.x = [];
    this.y = [];
    this.window = window;

    this.update(x, y);
  }

  public update(x: number[], y: number[]) {
    // 1. We check the new traces for new data.
    // This is by checking the last recorded x (time) and get the slice from that.
    // We assume that the x (time) is always increasing.
    const index = this.x.length ? sortedSearch(x, this.maxX) : -1;

    // 2. Bookkeep the traces from the new data
    if (index !== -1) {
      x = x.slice(index);
      y = y.slice(index);
    }
    Array.prototype.push.apply(this.x, x);
    Array.prototype.push.apply(this.y, y);

    // 3. Drop data when the window is filled to avoid overfilling the buffers
    const minIndex = this.minIndex;
    if (minIndex > 0) {
      // Update the `minIndex` and drop elements.
      // We use the `COUNT_LIMIT` just in case the first window position is not found.
      const dropIndex =
        Math.min(this.x.length - this.COUNT_LIMIT - 1, minIndex) + 1;

      // Only retain values after the `dropIndex`
      this.x.splice(0, dropIndex);
      this.y.splice(0, dropIndex);
    }
  }

  get COUNT_LIMIT() {
    return HISTORY_LIMIT;
  }

  get minIndex() {
    return sortedIndex(this.x, this.maxX - this.window);
  }

  get windowData(): { x: number[]; y: number[] } {
    const minX = this.maxX - this.window;
    let index = minX > 0 ? this.minIndex : 0;
    if (index !== 0) {
      ++index;
    }
    return {
      x: this.x.slice(index),
      y: this.y.slice(index),
    };
  }

  get data(): { x: number[]; y: number[] } {
    // We need to create new arrays to pass to the Plotly component as there
    // is a weird bug that the data is not rendered otherwise.
    return {
      x: [...this.x],
      y: [...this.y],
    };
  }

  get minX(): number {
    return this.x[0] ?? 0;
  }

  get maxX(): number {
    return this.x[this.x.length - 1] ?? 0;
  }
}

class TraceHistoryContainer {
  public traces: { [attribute: string]: TraceHistory };
  public window: number; // in seconds

  constructor(window: number) {
    this.traces = {};
    this.window = window;
  }

  public update(traces: Trace[]) {
    traces.forEach((trace) => {
      if (!this.traces[trace.fullName]) {
        // Add a new trace
        this.traces[trace.fullName] = new TraceHistory({
          x: trace.x ?? [],
          y: trace.y ?? [],
          window: this.window,
        });
      } else {
        // Update existing trace
        this.traces[trace.fullName].update(trace.x ?? [], trace.y ?? []);
      }
    });
  }

  get range(): number[] {
    return Object.values(this.traces).reduce(
      (acc, curr) => [Math.min(acc[0], curr.minX), Math.max(acc[1], curr.maxX)],
      [Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER]
    );
  }

  get windowRange(): number[] {
    const range = this.range;
    return [
      Math.max(range[0], range[1] - this.window),
      Math.max(range[1], this.window),
    ];
  }
}

export default function Plot(props: PlotProps) {
  const [mode, setMode] = useState("TIME_WINDOW");
  const { traces, params } = props;
  const {
    staticMode,
    width,
    height,
    showZeroLine,
    logarithmic,
    textColor,
    backgroundColor,
  } = params;

  // Create a reference for the trace history object
  const historyRef = useRef(new TraceHistoryContainer(params.timeWindow));
  const history = historyRef.current
  history.update(traces);

  // Get data
  const data = traces.map((trace) => {
    const yaxis = trace.axisLocation === "left" ? "y1" : "y2";
    const fullName = showHideTangoDBName(
      true,
      params.showTangoDB,
      trace.fullName
    );
    const hist = history.traces[trace.fullName];

    return {
      // Use the window range or the whole data depending on view mode
      ...(mode === "TIME_WINDOW" ? hist.windowData : hist.data),
      name: fullName,
      yaxis,
      mode: "lines+markers",
      marker: {
        color: trace.lineColor,
        size: 5,
      },
      line: {
        color: trace.lineColor,
        width: 1,
      },
    };
  });

  // Get layout
  const xaxis = {
    range: mode === "TIME_WINDOW" ? history.windowRange : history.range,
    title: "Time (s)",
    titlefont: { size: 12 },
    zeroline: false,
  };

  const zeroline = showZeroLine !== false;
  const hasRight = data.find(({ yaxis }) => yaxis === "y2") != null;
  const hasLeft =
    hasRight === false || data.find(({ yaxis }) => yaxis === "y1") != null;

  const addY1 = hasLeft
    ? {
        yaxis: {
          side: "left",
          showgrid: false,
          zeroline,
          type: logarithmic ? "log" : "",
        },
      }
    : {};
  const addY2 = hasRight
    ? {
        yaxis2: {
          side: "right",
          overlaying: "y",
          showgrid: false,
          zeroline,
        },
      }
    : {};

  const layout = {
    font: {
      family: "Helvetica, Arial, sans-serif",
      color: textColor,
    },
    paper_bgcolor: backgroundColor,
    plot_bgcolor: backgroundColor,
    xaxis,
    margin: {
      l: 30,
      r: 30,
      t: 15,
      b: 35,
    },
    autosize: true,
    showlegend: props.mode === "edit" ? false : true, //the legend has been disabled in edit mode because it generates problems when dropped in a boxwidget with click handler
    uirevision: true,
    legend: {
      y: 1.2,
      orientation: "h",
    },
    ...addY1,
    ...addY2,
  };

  const overriding = {
    ...addY1,
    ...addY2,
    width,
    height,
  };
  return (
    <Suspense fallback={null}>
      <Plotly
        onRelayout={(e: any) =>
          e["xaxis.autorange"] ? setMode("HISTORY") : setMode("TIME_WINDOW")
        }
        data={data}
        layout={{ ...layout, ...overriding }}
        config={{ staticPlot: staticMode === true }}
        responsive={true}
        style={{ width: "100%", height: "100%" }}
      />
    </Suspense>
  );
}

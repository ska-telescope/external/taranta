import React, { useCallback, useEffect, useState } from "react";

import Plot, { Trace, sortedSearch } from "./Plot";
import { WidgetProps } from "../types";
import {
  BooleanInputDefinition,
  ComplexInputDefinition,
  AttributeInputDefinition,
  SelectInputDefinition,
  NumberInputDefinition,
  ColorInputDefinition,
  AttributeInput,
} from "../../types";
import { useStore } from "react-redux";
import {
  getAttributeTimestampFromState,
  getAttributeValuesFromState,
} from "../../../shared/utils/getValuesHelper";

type Props = WidgetProps<Inputs>;

export interface AttributeComplexInput {
  attribute: AttributeInputDefinition;
  yAxis: SelectInputDefinition<"left" | "right">;
  showAttribute: SelectInputDefinition;
  lineColor: ColorInputDefinition;
}

export type Inputs = {
  timeWindow: NumberInputDefinition;
  showZeroLine: BooleanInputDefinition;
  logarithmic: BooleanInputDefinition;
  attributes: ComplexInputDefinition<AttributeComplexInput>;
  showTangoDB: BooleanInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
};

export interface TimeSeriesData {
  [name: string]: {
    timestamp: number[];
    values: number[];
  };
}

export const getAttributeFullName = (a) => {
  return Object.keys(a).length ? `${a.device}/${a.attribute}` : '';
};

export const getTrace = ({ attribute, data, t0 = 0 }): Trace => {
  let x: number[] = [];
  let y: number[] = [];

  const fullName = getAttributeFullName(attribute.attribute);
  if (
    data[fullName] &&
    Array.isArray(data[fullName].timestamp) &&
    Array.isArray(data[fullName].values)
  ) {
    const startIndex = sortedSearch(data[fullName].timestamp ?? [], t0);
    if (startIndex !== -1) {
      x = data[fullName].timestamp.slice(startIndex).map((t: number) => t - t0);
      y = data[fullName].values.slice(startIndex);
    }
  }

  return {
    fullName,
    x,
    y,
    axisLocation: attribute.yAxis,
    lineColor: attribute.lineColor,
  };
};

export const useThrottledData = ({
  attributes,
  active = false,
  delay = 500,
}: {
  attributes: AttributeInput[];
  active?: boolean;
  delay?: number;
}): TimeSeriesData => {
  const store = useStore();
  const [data, setData] = useState<TimeSeriesData>({});

  const update = useCallback(() => {
    // Define the background task
    const messages = store.getState().messages;
    const timestamps = getAttributeTimestampFromState(messages, attributes);
    const values = getAttributeValuesFromState(messages, attributes);

    // Get the new time series data from the attributes
    setData((currentData) => {
      const newData = attributes.reduce((acc, attr) => {
        const name = getAttributeFullName(attr);
        if (name) {
          // Naive check: we only check if the last received timestamp differs
          // from the bookkeeped data
          const timestamp = timestamps[name]?.timestamp ?? [];
          const hasNewValue =
            currentData[name]?.timestamp === undefined ||
            timestamp.at(-1) !== currentData[name]?.timestamp.at(-1);
          if (hasNewValue) {
            acc[name] = {
              timestamp,
              values: values[name]?.values,
            };
          }
        }
        return acc;
      }, {});

      // Only set data when there is new data
      return Object.keys(newData).length !== 0
        ? { ...currentData, ...newData }
        : currentData;
    });
  }, [attributes, store]);

  useEffect(() => {
    if (!active) {
      // Don't initialize the background task.
      return;
    }
    // Initialize the background task by setting up the interval
    // and at least calling once.
    const interval = setInterval(update, delay);
    update();

    // Return a closure to properly close the background task
    return () => clearInterval(interval);
  }, [active, delay, update]);

  return data;
};

function AttributePlot(props: Props) {
  const { mode, inputs, actualWidth, actualHeight } = props;
  const {
    attributes,
    timeWindow,
    showZeroLine,
    logarithmic,
    showTangoDB,
    textColor,
    backgroundColor,
  } = inputs;

  const runParams = {
    width: actualWidth,
    height: actualHeight,
    timeWindow,
    showZeroLine,
    logarithmic,
    showTangoDB,
    textColor,
    backgroundColor,
  };

  const staticParams = { ...runParams, staticMode: true };

  // Use throttled data selector to access attribute timestamp and values from the store
  const data = useThrottledData({
    attributes: attributes.map((attr) => attr.attribute),
    active: mode === "run",
  });

  if (mode === "run") {
    return (
      <Plot
        traces={attributes.map((attribute) =>
          getTrace({ attribute, data, t0: props.t0 })
        )}
        params={runParams}
        mode={mode}
        showTangoDB={showTangoDB}
      />
    );
  }

  if (mode === "library") {
    const xValues = Array(timeWindow)
      .fill(0)
      .map((_, i) => i);
    const sample1 = xValues.map((x) => 8 * Math.sin(x / 6) * Math.sin(x / 20));
    const sample2 = xValues.map((x) => 5 * Math.cos(x / 20) * Math.cos(x / 3));
    const traces: Trace[] = [
      {
        fullName: "attribute 1",
        x: xValues,
        y: sample1,
        axisLocation: "left",
        lineColor: "#1f77b4",
      },
      {
        fullName: "attribute 2",
        x: xValues,
        y: sample2,
        axisLocation: "left",
        lineColor: "#ff7f0e",
      },
    ];

    return (
      <Plot
        traces={traces}
        params={{ ...staticParams, height: 150 }}
        mode={mode}
        showTangoDB={showTangoDB}
      />
    );
  } else {
    const traces = attributes.map((attributeInput) => {
      const { device, attribute, label } = attributeInput.attribute;
      const { showAttribute } = attributeInput;

      let display = "";
      if (showAttribute === "Label") {
        if (label !== "") display = label;
        else display = "attributeLabel";
      } else if (showAttribute === "Name") {
        if (attribute !== null) display = attribute;
        else display = "attributeName";
      }

      const fullName = `${device || "?"}//${display || "?"}`;
      const trace: Trace = {
        fullName,
        axisLocation: attributeInput.yAxis,
        lineColor: attributeInput.lineColor,
      };
      return trace;
    });

    return (
      <Plot
        traces={traces}
        params={staticParams}
        mode={props.mode}
        showTangoDB={showTangoDB}
      />
    );
  }
}

export default AttributePlot;

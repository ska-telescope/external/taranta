import React, { useState } from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import { random, range, times } from "lodash";

import Plot, { Plotly, TraceHistory } from "./Plot";

configure({ adapter: new Adapter() });

const HISTORY_COUNT_LIMIT = 500;

beforeAll(() => {
  Object.defineProperty(TraceHistory.prototype, "COUNT_LIMIT", {
    get() {
      return HISTORY_COUNT_LIMIT;
    },
  });
});

// Mock useState
jest.mock("react", () => {
  const original = jest.requireActual("react");
  return {
    ...original,
    useState: jest.fn(),
  };
});
const useStateMock = useState as jest.Mock;

// Setup test data
const baseTraces = [
  {
    x: [],
    y: [],
    fullName: "first",
    axisLocation: "right",
    yAxisDisplay: "yAxis",
    position: 10,
    lineColor: "black",
  },
  {
    x: [],
    y: [],
    fullName: "second",
    axisLocation: "right",
    yAxisDisplay: "yAxis",
    position: 10,
    lineColor: "black",
  },
];

const numFirst = 1000;
const first = {
  x: range(numFirst),
  y: times(numFirst, () => random(1, 100)),
};

const numSecond = 600;
const second = {
  x: range(numSecond),
  y: times(numSecond, () => random(1, 100)),
};

const getTraces = (traces) =>
  baseTraces.map((base) => {
    // Extract the data for the current trace based on its fullName
    const trace = traces[base.fullName];
    return trace
      ? {
          ...base,
          x: trace.x,
          y: trace.y,
        }
      : base;
  });

const getRunParams = (params = {}) => {
  const base = {
    width: 100,
    height: 100,
    timeWindow: 120,
    showZeroLine: true,
    logarithmic: false,
    showTangoDB: false,
    textColor: "",
    backgroundColor: "cyan",
  };
  return { ...base, ...params };
};

// Simple conversion of traces to Plotly data.
const tracesToPlotlyData = (traces) =>
  traces.map((trace) => ({
    x: trace.x,
    y: trace.y,
    name: trace.fullName,
    yaxis: trace.axisLocation === "right" ? "y2" : "y1",
    mode: "lines+markers",
    marker: {
      color: trace.lineColor,
      size: 5,
    },
    line: {
      color: trace.lineColor,
      width: 1,
    },
  }));

const getMinX = (traces) =>
  traces.reduce((acc, obj) => Math.min(acc, Math.min(...obj.x)), Infinity);

const getMaxX = (traces) =>
  traces.reduce((acc, obj) => Math.max(acc, Math.max(...obj.x)), -Infinity);

describe("AttributePlotValues", () => {
  beforeEach(() => {
    useStateMock.mockImplementation((initial) => ["TIME_WINDOW", jest.fn()]);
  });

  it("passes the correct Plotly data and range with valid traces", () => {
    const runParams = getRunParams();
    const traces = getTraces({
      first: { x: [1, 2, 3], y: [10, 20, 30] },
      second: {
        x: [11, 12, 13],
        y: [110, 120, 130],
      },
    });
    const element = shallow(
      <Plot
        traces={traces}
        params={runParams}
        mode={"run"}
        showTangoDB={runParams.showTangoDB}
      />
    );

    const props: any = element.find(Plotly).props();
    expect(props.data).toEqual(tracesToPlotlyData(traces));
    expect(props.layout.xaxis.range).toEqual([
      getMinX(traces),
      runParams.timeWindow,
    ]);
  });

  it("passes the correct Plotly data and range with empty traces", () => {
    const runParams = getRunParams();
    const element = shallow(
      <Plot
        traces={baseTraces}
        params={runParams}
        mode={"run"}
        showTangoDB={runParams.showTangoDB}
      />
    );

    const props: any = element.find(Plotly).props();
    expect(props.data).toEqual(tracesToPlotlyData(baseTraces));
    expect(props.layout.xaxis.range).toEqual([0, runParams.timeWindow]);
  });

  it("accounts for the history limit (time window mode)", () => {
    // Set a suffiently large time window to receive all the history
    const timeWindow = 700;
    const runParams = getRunParams({ timeWindow });
    const inputTraces = getTraces({ first, second });

    // Render the component and get the child props
    const element = shallow(
      <Plot
        traces={inputTraces}
        params={runParams}
        mode={"run"}
        showTangoDB={runParams.showTangoDB}
      />
    );
    const props: any = element.find(Plotly).props();

    // Prepare and check with expected data.
    // This is just the `last HISTORY_LIMIT_COUNT` of the passed trace
    const expectedData = {
      // The data count is higher than the history count limit and the time window;
      // We slice it until the time window
      first: {
        x: first.x.slice(-timeWindow),
        y: first.y.slice(-timeWindow),
      },
      // The data count is lower than the history count limit but higher than the time window;
      // We return everything as we respect the timeWindow
      second,
    };
    const expectedTraces = getTraces(expectedData);
    expect(props.data).toEqual(tracesToPlotlyData(expectedTraces));

    const maxX = getMaxX(expectedTraces);
    const minX = maxX - timeWindow;
    expect(props.layout.xaxis.range).toEqual([minX, maxX]);
  });

  it("accounts for the history limit (history mode)", () => {
    // Setup history mode
    useStateMock.mockImplementation((initial) => ["HISTORY", jest.fn]);

    // Set a suffiently large time window to receive all the history
    const timeWindow = 700;
    const runParams = getRunParams({ timeWindow });
    const inputTraces = getTraces({ first, second });

    // Render the component and get the child props
    const element = shallow(
      <Plot
        traces={inputTraces}
        params={runParams}
        mode={"run"}
        showTangoDB={runParams.showTangoDB}
      />
    );
    const props: any = element.find(Plotly).props();

    // Prepare and check with expected data.
    // This is just the `last HISTORY_LIMIT_COUNT` of the passed trace
    const expectedData = {
      // The data count is higher than the history count limit and the time window;
      // We slice it until the time window
      first: {
        x: first.x.slice(-timeWindow),
        y: first.y.slice(-timeWindow),
      },
      // The data count is lower than the history count limit but higher than the time window;
      // We return everything as we respect the timeWindow
      second,
    };
    const expectedTraces = getTraces(expectedData);
    expect(props.data).toEqual(tracesToPlotlyData(expectedTraces));

    const maxX = getMaxX(expectedTraces);
    const minX = getMinX(expectedTraces);
    expect(props.layout.xaxis.range).toEqual([minX, maxX]);
  });
});

describe("TraceHistory", () => {
  it("creates a copy of the input arrays", () => {
    const x = [1, 2, 3];
    const y = [4, 5, 6];
    const history = new TraceHistory({ x, y, window: 100 });

    expect(history.x).not.toBe(x);
    expect(history.x).toEqual(x);

    expect(history.y).not.toBe(y);
    expect(history.y).toEqual(y);
  });

  it("updates the properties with the new arrays", () => {
    const oldX = [1, 2, 3];
    const oldY = [4, 5, 6];
    const history = new TraceHistory({ x: oldX, y: oldY, window: 100 });

    const newX = [10, 20, 30];
    const newY = [40, 50, 60];
    history.update(newX, newY);

    expect(history.x).toEqual([...oldX, ...newX]);
    expect(history.y).toEqual([...oldY, ...newY]);
  });

  it("drops elements when the count limit is exceeded and the window is smaller", () => {
    const history = new TraceHistory({ ...first, window: 100 });
    expect(history.x).toEqual(first.x.slice(-HISTORY_COUNT_LIMIT));
    expect(history.y).toEqual(first.y.slice(-HISTORY_COUNT_LIMIT));
  });

  it("doesn't drop elements when the count limit is exceeded and the window is larger", () => {
    const history = new TraceHistory({ ...first, window: 1000000 });
    expect(history.x).toEqual(first.x);
    expect(history.y).toEqual(first.y);
  });

  it("gives the right window data", () => {
    const window = 100;
    const history = new TraceHistory({ ...first, window });
    expect(history.windowData).toEqual({
      x: first.x.slice(-window),
      y: first.y.slice(-window),
    });
  });
});

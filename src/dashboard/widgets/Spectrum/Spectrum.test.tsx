import React from "react";
import { AttributeInput } from "../../types";
import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import Spectrum from "./Spectrum";
import SpectrumValues from "./SpectrumValues";

interface Input {
  attribute: AttributeInput;
  showTitle: boolean;
  showTangoDB: boolean;
  inelastic: boolean;
  showAttribute: string;
  showSpecificYIndexValue: string;
  timeWindow: number;
}

configure({ adapter: new Adapter() });

jest.mock("./SpectrumValues", () => () => <div>Mock SpectrumValues</div>);

describe("Spectrum Tests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  let writeArray: any = [];
  let date = new Date();
  let timestamp = date.getTime();

  beforeEach(() => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      dataType: "DevBoolean",
      dataFormat: "spectrum",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      writeValue: "",
      label: "TimeRange",
      history: [],
      timestamp: timestamp,
      quality: "",
    };
    myInput = {
      attribute: myAttributeInput,
      showTitle: true,
      showTangoDB: false,
      inelastic: true,
      timeWindow: 120,
      showAttribute: "Name",
      showSpecificYIndexValue: "1", 
      
    };
  });

  it("check if SpectrumValues component is invoked", () => {
    const element = React.createElement(Spectrum.component, {
      mode: "library",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(SpectrumValues).exists()).toBe(true);
    // Optional: Check if showSpecificYIndexValue is passed correctly
    expect(shallowElement.find(SpectrumValues).prop('showSpecificYIndexValue')).toBe("1");
  });

  it("check if SpectrumValues component is invoked with showAttribute Label", () => {

    myInput.showAttribute = "Label";

    const element = React.createElement(Spectrum.component, {
      mode: "library",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(SpectrumValues).exists()).toBe(true);
  });
});

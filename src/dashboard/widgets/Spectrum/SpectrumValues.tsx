import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { getAttributeLastValueFromState } from "../../../shared/utils/getLastValueHelper";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import PlotlyCore from "plotly.js/lib/core";
import PlotlyScatter from "plotly.js/lib/scatter";
import createPlotlyComponent from "react-plotly.js/factory";

const sampleData = [0, -2, 3, -2, 1, -5, 4, -3, -2, -4, 0, -4, 2, 2, -2, -2, 2, -5, -2, -3, 0];

const SpectrumValues = ({
  deviceName,
  attributeName,
  mode,
  title,
  titlefont,
  font,
  margin,
  autosize,
  timeWindow,
  inelastic,
  config,
  responsive,
  style,
  showSpecificYIndexValue,
  showSpecificXIndexValue, // New prop for X-axis index
}) => {
  PlotlyCore.register([PlotlyScatter]);
  const Plotly = createPlotlyComponent(PlotlyCore);

  const value = useSelector((state: IRootState) => {
    return getAttributeLastValueFromState(
      state.messages,
      deviceName,
      attributeName
    );
  });

  const [minValue, setMinValue] = useState(0);
  const [maxValue, setMaxValue] = useState(10);
  const [history, setHistory] = useState<number[]>([]); // State to store Y history
  const [xHistory, setXHistory] = useState<number[]>([]); // State to store X history

  const { min: currMin, max: currMax } = { min: minValue, max: maxValue };

  const yAxis = (inelastic) => {
    const { min, max } = { min: minValue, max: maxValue };
    if (inelastic && min != null && max != null) {
      return {
        range: [1.1 * min, 1.1 * max],
      };
    } else {
      return {};
    }
  };

  let y = mode === "library" || value === undefined ? sampleData : value;

  // Handle specific index selection for both Y and X axes
  useEffect(() => {
    const yIndex = parseInt(showSpecificYIndexValue);
    const xIndex = parseInt(showSpecificXIndexValue);

    // Update history for Y axis
    if (!isNaN(yIndex) && y[yIndex] !== undefined) {
      setHistory((prevHistory) => {
        const newHistory = [...prevHistory, y[yIndex]];
        if (newHistory.length > timeWindow) newHistory.shift();
        return newHistory;
      });
    }

    // Update history for X axis
    if (!isNaN(xIndex) && y[xIndex] !== undefined) {
      setXHistory((prevHistory) => {
        const newHistory = [...prevHistory, y[xIndex]];
        if (newHistory.length > timeWindow) newHistory.shift();
        return newHistory;
      });
    }
  }, [y, showSpecificYIndexValue, showSpecificXIndexValue, timeWindow]);

  // Prepare data for Plotly
  const data = [
    {
      x: showSpecificXIndexValue ? xHistory : Array.from(Array(y.length).keys()),
      y: showSpecificYIndexValue ? history : y,
    },
  ];

  const propMin = y && y.length > 0 ? y.reduce((a, b) => Math.min(a, b)) : 0;
  const propMax = y && y.length > 0 ? y.reduce((a, b) => Math.max(a, b)) : 0;
  const min = propMin;
  const max = propMax;

  if (min !== currMin || max !== currMax) {
    setMinValue(min);
    setMaxValue(max);
  }

  const yaxis = yAxis(inelastic);

  const layout = {
    title,
    titlefont: titlefont,
    font: font,
    margin: margin,
    autosize: autosize,
    yaxis,
  };

  return (
    <div id="plotlySpectrumDiv">
      <Plotly
        data={data}
        layout={layout}
        config={config}
        responsive={responsive}
        style={style}
      />
    </div>
  );
};

export default SpectrumValues;

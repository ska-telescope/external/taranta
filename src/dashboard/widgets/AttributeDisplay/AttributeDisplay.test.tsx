import React from "react";
import { AttributeInput } from "../../types";
import "../../../shared/tests/globalMocks";
import { configure, mount } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import AttributeDisplay from "./AttributeDisplay";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";

jest.mock("./AttributeValues", () => () => <div>Mock AttributeValues</div>);

configure({ adapter: new Adapter() });
const store = configureStore();

interface Input {
  showDevice: boolean;
  showTangoDB: boolean;
  showAttribute: string;
  showUnit: boolean;
  jsonCollapsed: boolean;
  scientificNotation: boolean;
  precision: number;
  format: string;
  showEnumLables: boolean;
  showAttrQuality: boolean;
  attribute: AttributeInput;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  showEnumLabels: boolean;
  alignTextCenter: boolean;
  alignValueRight: boolean;
  widgetCss: string;
}

describe("AttributeDisplayTests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("renders all false without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "",
      writeValue: "",
      quality: "",
      timestamp: timestamp,
    };

    myInput = {
      showDevice: false,
      showTangoDB: true,
      showAttribute: "",
      showUnit: false,
      jsonCollapsed: false,
      scientificNotation: false,
      precision: 2,
      format: "",
      showEnumLables: false,
      showAttrQuality: false,
      attribute: myAttributeInput,
      textColor: "",
      backgroundColor: "",
      size: 1,
      font: "",
      showEnumLabels: false,
      alignTextCenter: false,
      alignValueRight: false,
      widgetCss: "",
    };
    let element = React.createElement(AttributeDisplay.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#AttributeDisplay").exists()).toBe(true);
  });

  it("renders all true without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 123,
      writeValue: "",
      quality: "valid",
      timestamp: timestamp,
    };

    myInput = {
      showDevice: true,
      showTangoDB: true,
      showAttribute: "Name",
      showUnit: true,
      jsonCollapsed: false,
      scientificNotation: false,
      precision: 2,
      format: "",
      showEnumLables: true,
      showAttrQuality: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
      alignTextCenter: false,
      alignValueRight: false,
      widgetCss: "",
    };

    let element = React.createElement(AttributeDisplay.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#AttributeDisplay").exists()).toBe(true);
    myInput = {
      showDevice: true,
      showTangoDB: true,
      showAttribute: "Name",
      showUnit: true,
      jsonCollapsed: false,
      scientificNotation: true,
      precision: 2,
      format: "",
      showEnumLables: true,
      showAttrQuality: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
      alignTextCenter: false,
      alignValueRight: false,
      widgetCss: "",
    };

    element = React.createElement(AttributeDisplay.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
    wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#AttributeDisplay").exists()).toBe(true);
  });
});

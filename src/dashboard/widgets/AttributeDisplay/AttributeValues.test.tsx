import React from "react";
import { render, act } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import AttributeValues from "./AttributeValues";
import {
  getAttributeLastQualityFromState as getAttributeLastQualityFromStateMock,
  getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));

const getAttributeLastQualityFromState = getAttributeLastQualityFromStateMock as jest.Mock;
const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
  getAttributeLastQualityFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("AttributeValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {},
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockReturnValue("1");
    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    getAttributeLastQualityFromState.mockReturnValue("ATTR_INVALID");
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render the attribute value", () => {
    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="m/s"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.textContent).toContain("1");
    expect(container.textContent).toContain("m/s");
  });

  it("should render the attribute value with quality but in edit mode", () => {
    getAttributeLastQualityFromState.mockReturnValue("ATTR_VALID");

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="edit"
        showAttrQuality={true}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="m/s"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.textContent).not.toContain("VALID");
  });

  it("should render the attribute value with quality", () => {
    getAttributeLastQualityFromState.mockReturnValue("ATTR_VALID");

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={true}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="m/s"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.textContent).toContain("VALID");
  });

  it("should render the attribute value with minAlarm and maxAlarm and alarm ON", () => {
    getAttributeLastValueFromState.mockReturnValue("1");

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="A"
        maxAlarm={5}
        minAlarm={2}
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("rgb(255, 170, 170)");
  });

  it("should render the attribute value with minAlarm and maxAlarm and alarm OFF", () => {
    getAttributeLastValueFromState.mockReturnValue("3");

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="A"
        maxAlarm={5}
        minAlarm={2}
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).not.toContain("rgb(255, 170, 170)");
  });

  it("should render the attribute value with JSON", () => {
    getAttributeLastValueFromState.mockReturnValue(
      `{ "routes": [
        { "src": { "channel": 48 }, "dst": { "port": 5 } },
        { "src": { "channel": 437 }, "dst": { "port": 19 } }
      ] }`
    );

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("pretty-json-container");
  });

  it("should render an error using a wrong xPath", () => {
    getAttributeLastValueFromState.mockReturnValue(
      `{ "routes": [ 
        { "src": { "channel": 48 }, "dst": { "port": 5 } }, 
        { "src": { "channel": 437 }, "dst": { "port": 19 } } 
      ] }`
    );

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={"routes["}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("Error evaluating JSON expression");
  });

  it("should render a message if used the xPath query has no result", async () => {
    getAttributeLastValueFromState.mockReturnValue(
      `{ "routes": [ 
        { "src": { "channel": 48 }, "dst": { "port": 5 } }, 
        { "src": { "channel": 437 }, "dst": { "port": 19 } } 
      ] }`
    );

    let container;
    // @ts-ignore
    await act(async () => {
      container = render(
        <AttributeValues
          attributeName="attr"
          deviceName="device"
          mode="run"
          showAttrQuality={false}
          precision={2}
          format={"routes[11]"}
          showUnit={true}
          scientificNotation={false}
          showEnumLabels={false}
          jsonCollapsed={false}
          enumlabels={[]}
          unit=""
          alignValueRight={false}
          dataType="DevString"
        />
      ).container;
    });

    expect(container.innerHTML).toContain(
      "No result evaluating JSON expression"
    );
  });

  it("should render a value inside JSON using xPath", async () => {
    getAttributeLastValueFromState.mockReturnValue(
      `{ "routes": [ 
        { "src": { "channel": 48 }, "dst": { "port": 5 } }, 
        { "src": { "channel": 437 }, "dst": { "port": 19 } } 
      ] }`
    );

    let container;
    // @ts-ignore
    await act(async () => {
      container = render(
        <AttributeValues
          attributeName="attr"
          deviceName="device"
          mode="run"
          showAttrQuality={false}
          precision={2}
          format={"routes[0].src.channel"}
          showUnit={true}
          scientificNotation={false}
          showEnumLabels={false}
          jsonCollapsed={false}
          enumlabels={[]}
          unit=""
          alignValueRight={false}
          dataType="DevString"
        />
      ).container;
    });

    expect(container.innerHTML).toContain("48");
    expect(container.innerHTML).not.toContain("channel");
  });

  it("should render the attribute value with number and show enumlables", () => {
    getAttributeLastValueFromState.mockReturnValue(1);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        scientificNotation={false}
        showUnit={true}
        showEnumLabels={true}
        jsonCollapsed={false}
        enumlabels={["ON", "OFF"]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("OFF");
  });

  it("should render the attribute value with number and show scientificNotation", () => {
    getAttributeLastValueFromState.mockReturnValue(1000);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={true}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={["ON", "OFF"]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("1.00e+3");
  });

  it("should render the attribute value with number and precision 2", () => {
    getAttributeLastValueFromState.mockReturnValue(10.12345);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={2}
        format={""}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={["ON", "OFF"]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("10.12");
  });

  it("should render the attribute value with number and precision 3 using format and ignoring the precision field, rounding the number", () => {
    getAttributeLastValueFromState.mockReturnValue(1234.5678);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={"0.00"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={["ON", "OFF"]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("1234.57");
  });

  it("should render the attribute value with number and precision 3 and thousand separators using format and ignoring the scientificNotation field, rounding the number", () => {
    getAttributeLastValueFromState.mockReturnValue(1234.5678);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={"0,000.00"}
        showUnit={true}
        scientificNotation={true}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={["ON", "OFF"]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("1,234.57");
  });

  it("should render the attribute value with scientific Notation using format", () => {
    getAttributeLastValueFromState.mockReturnValue(1234.5678);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={"0,0e+0"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={["ON", "OFF"]}
        unit="A"
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("1e+3");
  });

  it("should render the attribute value as binary using sprintf-js library", () => {
    getAttributeLastValueFromState.mockReturnValue(100);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={"%b"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit=""
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("1100100");
  });

  it("should render the attribute value as Sexagesimal using util library", () => {
    getAttributeLastValueFromState.mockReturnValue(10.9);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={"%s"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit=""
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("10:54:0.0.000");
  });

  it("should render the attribute value multiplied by 100 using numeral-js library", () => {
    getAttributeLastValueFromState.mockReturnValue(100);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={".multiply(100)"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit=""
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("10000");
  });

  it("should render the attribute value reduced by 10 using numeral-js library", () => {
    getAttributeLastValueFromState.mockReturnValue(100);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={".subtract(10)"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit=""
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("90");
  });

  it("should render an error message in case of wrong format", () => {
    getAttributeLastValueFromState.mockReturnValue(100);

    const { container } = render(
      <AttributeValues
        attributeName="attr"
        deviceName="device"
        mode="run"
        showAttrQuality={false}
        precision={3}
        format={"%a"}
        showUnit={true}
        scientificNotation={false}
        showEnumLabels={false}
        jsonCollapsed={false}
        enumlabels={[]}
        unit=""
        alignValueRight={false}
        dataType="DevString"
      />
    );
    expect(container.innerHTML).toContain("Format not valid");
  });
});

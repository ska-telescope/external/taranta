import React from 'react';
import { render } from '@testing-library/react';
import { useSelector as useSelectorMock } from 'react-redux';
import DashboardLinkExport from './DashboardLink';
import { fireEvent } from '@testing-library/react';
const { component: DashboardLink } = DashboardLinkExport;

const useSelector = useSelectorMock as jest.Mock;

// Mocking the useSelector hook from react-redux
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

describe('DashboardLink', () => {
  beforeEach(() => {
    // Mocking the return value of useSelector before each test
    useSelector.mockImplementation((selectorFn: any) => selectorFn({
      dashboards: {
        dashboards: [
          { name: 'Dashboard1' },
          { name: 'Dashboard2' },
        ],
      },
    }));
  });

  afterEach(() => {
    // Clear all mocks after each test
    jest.clearAllMocks();
  });

  it('should render link with correct text in run mode', () => {
    const { container } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard 1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    expect(container.textContent).toContain('Dashboard1Dashboard2OPEN');
  });

  it('should render link with correct text in run mode without dropdown', () => {
    const { container } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard 1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: true,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    expect(container.textContent).toContain('OPEN');
  });

  it('should render link with correct text in library mode', () => {
    const { container } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard 1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="library"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    expect(container.textContent).toContain('Dashboard 1OPEN');
  });

  it('should render link with correct text in edit mode', () => {
    const { container } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard 1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="edit"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    expect(container.textContent).toContain('Dashboard1Dashboard2OPEN');
  });

  it('should apply custom CSS when provided', () => {
    const { container } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard 1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: false,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: 'color: red;',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    expect(container.textContent).toContain('Dashboard1Dashboard2OPEN');
    expect(container.innerHTML).toContain('color: red');
  });

  it('should update selectedDashboard state on dropdown change', () => {
    const { getByRole } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard 1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    // Find the dropdown and simulate a change event
    const dropdown = getByRole('combobox');
    fireEvent.change(dropdown, { target: { value: 'Dashboard2' } });

    // Check if the dropdown's value has changed
    expect(dropdown['value']).toBe('Dashboard2');
  });

  it('should update myDashboardName based on ID if name does not match', () => {
    // Mocking the return value of useSelector for this specific test
    useSelector.mockImplementation((selectorFn: any) => selectorFn({
      dashboards: {
        dashboards: [
          { name: 'Dashboard1', id: '6536740cd6523e0012dc86e1' },
          { name: 'Dashboard2', id: '6536740cd6523e0012dc86e2' },
        ],
      },
    }));

    const { getByRole } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"NonExistentDashboard","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    // Find the dropdown and check its value
    const dropdown = getByRole('combobox');
    expect(dropdown['value']).toBe('Dashboard1');
  });

  it('should navigate to the correct dashboard URL on same tab on button click', () => {
    // Mocking the return value of useSelector for this specific test
    useSelector.mockImplementation((selectorFn: any) => selectorFn({
      dashboards: {
        dashboards: [
          { name: 'Dashboard1', id: '6536740cd6523e0012dc86e1' },
          { name: 'Dashboard2', id: '6536740cd6523e0012dc86e2' },
        ],
      },
    }));

    // Mock window.open
    const mockWindowOpen = jest.spyOn(window, 'open').mockImplementation(() => null);

    // Mock window.location.href
    let setHref = '';
    Object.defineProperty(window, 'location', {
      value: {
        set href(val) {
          setHref = val;
        },
        get href() {
          return setHref;
        }
      },
      writable: true,
    });

    const { getByText } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: false,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    // Simulate a button click
    fireEvent.click(getByText('OPEN'));

    // Check if window.location.href has been updated correctly
    expect(setHref).toBe('dashboard?id=6536740cd6523e0012dc86e1&mode=run');

    // Cleanup mocks
    mockWindowOpen.mockRestore();
  });

  it('should open the correct dashboard URL in a new tab on button click', () => {
    // Mocking the return value of useSelector for this specific test
    useSelector.mockImplementation((selectorFn: any) => selectorFn({
      dashboards: {
        dashboards: [
          { name: 'Dashboard1', id: '6536740cd6523e0012dc86e1' },
          { name: 'Dashboard2', id: '6536740cd6523e0012dc86e2' },
        ],
      },
    }));

    // Mock window.open
    const mockWindowOpen = jest.spyOn(window, 'open').mockImplementation(() => null);

    const { getByText } = render(
      <DashboardLink
        inputs={{
          DefaultDashboard: '{"name":"Dashboard1","id":"6536740cd6523e0012dc86e1"}',
          HideDropdown: false,
          NewTab: true,
          ButtonText: 'OPEN',
          ButtonCss: '',
          DropDownCss: '',
          CustomCss: '',
        }}
        mode="run"
        id={1}
        t0={1}
        actualWidth={100}
        actualHeight={100}
      />
    );

    // Simulate a button click
    fireEvent.click(getByText('OPEN'));

    // Check if window.open has been called with the correct URL
    expect(mockWindowOpen).toHaveBeenCalledWith('dashboard?id=6536740cd6523e0012dc86e1&mode=run', '_blank');

    // Cleanup mocks
    mockWindowOpen.mockRestore();
  });

});

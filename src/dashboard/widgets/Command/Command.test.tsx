import React from "react";

import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import Command from "./Command";
import { CommandInput } from "../../types";

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

jest.mock('../../../shared/utils/InputField', () => ({
  InputField: () => <div data-testid="mocked-draggable-modal" />,
}));

configure({ adapter: new Adapter() });

describe("Command Widget test cases", () => {
  it("renders without crashing", () => {
    let commandInputArray: CommandInput;

    commandInputArray = {
      device: "tangotest://sys/tg_test/1",
      command: "DevVarStringArray",
      output: "",
      execute: () => null,
    };
    const myInput: any = {
      title: 'Title - Command',
      uploadBtnLabel: '',
      buttonText: "Trigger On",
      command: commandInputArray,
      showDevice: true,
      showCommand: true,
      showTangoDB: true,
      requireConfirmation: false,
      displayOutput: true,
      alignSendButtonRight: false,
      outerDivCss: 'background-color: red',
      uploadButtonCss: '',
      sendButtonCss: '',
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      widgetCss: "background-color: green"
    };

    const element = React.createElement(Command.component, {
      mode: "run",
      t0: 1,
      id: 12,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    expect(shallow(element).html()).toContain("tangotest://sys/tg_test/1");
    expect(shallow(element).html()).toContain("DevVarStringArray");
    expect(shallow(element).html()).toContain("Title - Command");
    expect(shallow(element).html()).toContain("command-output");
    expect(shallow(element).html()).toContain("Helvetica");
    expect(shallow(element).html()).toContain("background-color:green");
 });

  it("renders with empty device command", () => {
    let commandInputArray;

    commandInputArray = {
      type: "command",
      device: "",
      acceptedType: "DevLong",
      command: "",
      output: "",
      execute: () => null,
    };
    const myInput: any = {
      title: 'Title - Command',
      uploadBtnLabel: '',
      buttonText: "",
      command: commandInputArray,
      showDevice: true,
      showCommand: true,
      requireConfirmation: false,
      displayOutput: true,
      alignSendButtonRight: false,
      outerDivCss: 'background-color: red',
      uploadButtonCss: '',
      sendButtonCss: '',
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
    };

    const element = React.createElement(Command.component, {
      mode: "run",
      t0: 1,
      id: 124,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    expect(shallow(element).html()).toContain("device");
    expect(shallow(element).html()).toContain("command:");
  });

  it("renders by hiding device and command", () => {
    let commandInputArray;

    commandInputArray = {
      type: "command",
      acceptedType: "DevLong",
      device: "",
      command: "",
      output: "",
      execute: () => null,
    };
    const myInput: any = {
      title: 'Title - Command',
      uploadBtnLabel: '',
      buttonText: "",
      command: commandInputArray,
      showDevice: false,
      showCommand: false,
      showTangoDB: false,
      requireConfirmation: false,
      displayOutput: true,
      alignSendButtonRight: false,
      outerDivCss: 'background-color: red',
      uploadButtonCss: '',
      sendButtonCss: '',
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
    };

    const element = React.createElement(Command.component, {
      mode: "run",
      t0: 1,
      id: 125,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    expect(shallow(element).html()).not.toContain("device");
    expect(shallow(element).html()).not.toContain("command:");
    expect(shallow(element).html()).not.toContain("font-family");
  });

  it("renders by hiding tangoDB name", () => {
    let commandInputArray;

    commandInputArray = {
      type: "command",
      acceptedType: "DevLong",
      device: "",
      command: "",
      output: "",
      execute: () => null,
    };
    const myInput: any = {
      title: 'Title - Command',
      uploadBtnLabel: '',
      buttonText: "",
      command: commandInputArray,
      showDevice: true,
      showCommand: false,
      showTangoDB: false,
      requireConfirmation: false,
      displayOutput: true,
      alignSendButtonRight: false,
      outerDivCss: 'background-color: red',
      uploadButtonCss: '',
      sendButtonCss: '',
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
    };

    const element = React.createElement(Command.component, {
      mode: "run",
      t0: 1,
      id: 125,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    expect(shallow(element).html()).not.toContain("tangotest");
  });
});

import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import CommandFile from "./CommandFile";

jest.mock("react-redux", () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

jest.mock("../../../shared/components/CommandArgsFile/CommandArgsFile", () => ({
  CommandArgsFile: () => <div data-testid="mocked-draggable-modal" />,
}));

configure({ adapter: new Adapter() });

describe("CommandFileTests", () => {
  it("renders all false without crashing", () => {
    const myInput = {
      title: "",
      uploadBtnLabel: "",
      buttonLabel: "",
      command: {
        type: "commandFile",
        device: "",
        command: "",
        intype: "",
        invalidates: [""],
        parameter: "",
        intypedesc: "",
        outtypedesc: "",
        outtype: "",
        displevel: "",
        output: "",
        execute: () => null,
      },
      showDevice: false,
      showTangoDB: false,
      showCommand: false,
      requireConfirmation: false,
      displayOutput: false,
      alignSendButtonRight: false,
      textColor: "",
      backgroundColor: "",
      size: 14,
      font: "",
      outerDivCss: "",
      uploadButtonCss: "",
      sendButtonCss: "",
    };

    const element = React.createElement(CommandFile.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let commandType = shallow(element)
      .children()
      .first()
      .get(0).props.command.type;
    expect(commandType).toStrictEqual("commandFile");
    //expect(shallow(element).html()).toContain("CommandFile");
  });
});

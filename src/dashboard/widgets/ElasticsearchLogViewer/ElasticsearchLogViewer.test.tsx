import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import WrapperElasticsearchLogViewer, { ConnectedElasticsearchLogViewer } from "./ElasticsearchLogViewer";
import {
  DashboardEditHistory,
  Dashboard
} from "../../types";

interface Inputs {
  overflow: boolean;
  textDisplay: boolean;
  searchText: string;
  fromDate: Date;
  toDate: Date;
  deviceSelection: any[];
  logLevel: any[];
  refresh: string;
  customCss: string;
}
configure({ adapter: new Adapter() });
const store = configureStore();
let mySelect = [{ name: "name", value: "value" }]

let myInput: Inputs;

myInput = {
  overflow: true,
  textDisplay: true,
  searchText: "searchText",
  fromDate: new Date(),
  toDate: new Date(),
  deviceSelection: mySelect,
  logLevel: mySelect,
  refresh: "5000",
  customCss: "customCss",
}

const widgets = [
  {
    _id: '607e8e9c99ba3e0017cf9080',
    id: '1',
    x: 16,
    y: 5,
    canvas: '0',
    width: 14,
    height: 3,
    type: 'ATTRIBUTE_DISPLAY',
    inputs: {
      attribute: {
        device: 'CSPVar1',
        attribute: 'state',
        label: 'State'
      },
      precision: 2,
      showDevice: false,
      showAttribute: 'Label',
      scientificNotation: false,
      showEnumLabels: false,
      textColor: '#000000',
      backgroundColor: '#ffffff',
      size: 1,
      font: 'Helvetica'
    },
    order: 0,
    valid: 1
  },
  {
    _id: '607e946599ba3e0017cf90a0',
    id: '2',
    x: 16,
    y: 10,
    canvas: '0',
    width: 20,
    height: 3,
    type: 'COMMAND_ARRAY',
    inputs: {
      command: {
        device: 'myvar',
        command: 'DevString',
        acceptedType: 'DevString'
      },
      showDevice: true,
      showCommand: true,
      requireConfirmation: true,
      displayOutput: true,
      OutputMaxSize: 20,
      timeDisplayOutput: 3000,
      textColor: '#000000',
      backgroundColor: '#ffffff',
      size: 1,
      font: 'Helvetica'
    },
    order: 1,
    valid: 1
  }
];
let myHistory: DashboardEditHistory;
myHistory = {
  undoActions: [{ "1": widgets[0] }],
  redoActions: [{ "1": widgets[0] }],
  undoIndex: 1,
  redoIndex: 2,
  undoLength: 3,
  redoLength: 4,
};
const myDashboards: Dashboard[] = [
  {
    id: "604b5fac8755940011efeea1",
    name: "Untitled dashboard",
    user: "user1",
    environment: [],
    groupWriteAccess: true,
    selectedIds: ["aaa"],
    history: myHistory,
    insertTime: new Date("2021-03-12T12:33:48.981Z"),
    updateTime: new Date("2021-04-20T08:12:53.689Z"),
    group: null,
    lastUpdatedBy: "user1",
    variables: [
      {
        _id: "0698hln1j359a",
        name: "myvar",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
    ],
  },
  {
    id: "6073154703c08b0018111066",
    name: "Untitled dashboard",
    user: "user1",
    environment: [],
    groupWriteAccess: true,
    selectedIds: ["aaa"],
    history: myHistory,
    insertTime: new Date("2021-04-11T15:27:03.803Z"),
    updateTime: new Date("2021-04-20T08:44:21.129Z"),
    group: "aGroup",
    lastUpdatedBy: "user1",
    variables: [
      {
        _id: "78l4d190kh2g",
        name: "cspvar1",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
      {
        _id: "i5am90g1il0e",
        name: "myvar",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
    ],
  },
];
const selectedDashboard = { "selectedId": null, environment: [], "selectedIds": ["1"], "widgets": { "1": { "_id": "60910a16464196001183c1c4", "id": "1", "x": 16, "y": 3, "canvas": "0", "width": 13, "height": 4, "type": "PARAMETRIC_WIDGET", "inputs": { "name": "MName", "variable": "myvar" }, "order": 0, "valid": 1 } }, "id": "604b5fac8755940011efeea1", "name": "Untitled dashboard", "user": "user1", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "insertTime": new Date(), "updateTime": new Date(), "history": { "undoActions": [], "redoActions": [], "undoIndex": 0, "redoIndex": 0, "undoLength": 0, "redoLength": 0 }, "variables": [] }
const dashboards = [{ "id": "604b5fac8755940011efeea1", "name": "Untitled dashboard", "user": "user1", "insertTime": new Date(), "updateTime": new Date(), "group": null, "lastUpdatedBy": "user1", "tangoDB": "testdb", "variables": [{ "id": "0cef8i6340ij7", "name": "myvar", "class": "DServer", "device": "dserver/DataBaseds/2" }] }, { "id": "6073154703c08b0018111066", "name": "ajay", "user": "user1", "insertTime": "2021-04-11T15:27:03.803Z", "updateTime": "2021-05-04T13:01:40.904Z", "group": null, "lastUpdatedBy": "user1", "tangoDB": "testdb" }];

describe("Get all devices", () => {
  beforeEach(() => {
    // @ts-ignore
    delete window.location;
    // @ts-ignore
    window.location = new URL('https://k8s.stfc.skao.int/testdb/dashboard?id=64255c0fdd11d00018977284');
  });

  it("getAllDevices", () => {
    const launchElasticQuery = ConnectedElasticsearchLogViewer.WrappedComponent.prototype.launchElasticQuery = jest.fn();

    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      inputs: myInput,
      tangoDB: 'testDB',
      messages: {},
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '42',
      devices: [],
      elasticError: ""
    })

    shallow(element);
    expect(launchElasticQuery).toHaveBeenCalledTimes(1);
  })

  it("getAllDevices else condition", () => {

    const myInput1 = {
      overflow: true,
      textDisplay: true,
      searchText: "searchText",
      fromDate: new Date(),
      toDate: new Date(),
      // deviceSelection: mySelect,
      logLevel: mySelect,
      refresh: "5000",
      customCss: "customCss",
    }

    const launchElasticQuery = ConnectedElasticsearchLogViewer.WrappedComponent.prototype.launchElasticQuery = jest.fn();
    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      //@ts-ignore
      inputs: myInput1,
      tangoDB: 'testDB',
      messages: {},
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '42',
      devices: []
    })
    const shallowElement = shallow(element);
    expect(launchElasticQuery).toHaveBeenCalledTimes(1);
    shallowElement.find('[name="search-text"]').simulate('change', { target: {value: 'tg_test' }});
    expect(shallowElement.state('searchText')).toBe("tg_test");

    shallowElement.find('.from-date-selector').first().simulate('change', { target: {value: '2023-08-01 12:00:00.000+05:30' }});
    shallowElement.update();

    shallowElement.find('.to-date-selector').first().simulate('change', { target: {value: '2023-08-01 12:00:00.000+05:30' }});
    shallowElement.update();
  })
})

describe("Check for invalid url", () => {
  beforeEach(() => {
    // @ts-ignore
    delete window.location;
    // @ts-ignore
    window.location = new URL('https://abc.prq.int/testdb/dashboard?id=64255c0fdd11d00018977284');
  });

  it("render", () => {
    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      inputs: myInput,
      tangoDB: 'testDB',
      messages: {},
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '42',
      devices: [],
      elasticError: ""
    })

    const shallowElement = shallow(element);
    expect(shallowElement.html()).toContain('The current URL is not accepted to use this widget');
    expect(shallowElement.html()).toContain('elastic-disabled');
  })
})

describe("Test Elasticsearch log viewer", () => {

  beforeEach(() => {
    // @ts-ignore
    delete window.location;
    // @ts-ignore
    window.location = new URL('https://k8s.stfc.skao.int/testdb/dashboard?id=64255c0fdd11d00018977284');
  });

  it("it renders without crash in edit mode", () => {
    let element = React.createElement(WrapperElasticsearchLogViewer.component, {
      mode: "edit",
      inputs: myInput,
      tangoDB: 'testDB',
      id: '42',
    })

    const content = mount(<Provider store={store}>{element}</Provider>);
    content.setProps({
      username: "CREAM",
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
    });

    expect(content.html()).toContain("log-container");
    expect(content.html()).toContain('2022-07-05 16:38:54,409.409Z | INFO | test.py#1 | This is a info message');
  });

  it("it renders without crash in run mode", () => {
    let element = React.createElement(WrapperElasticsearchLogViewer.component, {
      mode: "run",
      inputs: {...myInput, textDisplay: false},
      tangoDB: 'testDB',
      id: '42',
    })

    const content = mount(<Provider store={store}>{element}</Provider>);
    content.setProps({
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
    });

    expect(content.html()).toContain("fa-pause-circle");
    expect(content.html()).toContain("Version");
    expect(content.html()).toContain("File");
  });

  it("it renders without crash in run mode with custom elasticsearchUrl", () => {
    let element = React.createElement(WrapperElasticsearchLogViewer.component, {
      mode: "edit",
      inputs: {...myInput, textDisplay: false},
      tangoDB: 'testDB',
      id: '42',
    })

    const content = mount(<Provider store={store}>{element}</Provider>);
    content.setProps({
      username: "CREAM",
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
    });

    expect(content.html()).toContain("fa-pause-circle");
    expect(content.html()).toContain('<th class="sticky-column">Version</th>');
  })

  it("check refresh rate", async () => {
    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      inputs: myInput,
      tangoDB: 'testDB',
      messages: {},
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '42',
      devices: ["dserver/databaseds/2", "sys/tg_test/1"],
      elasticError: "Failed to fetch log at 22-08-2023"
    })

    const shallowElement = shallow(element);
    shallowElement.setProps({
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
    });

    expect(shallowElement.html()).toContain('Failed to fetch log at 22-08-2023');
  });

  it("check refresh rate", async () => {
    myInput = {
      ...myInput,
      refresh: "1000",
    }

    const launchElasticQuery = ConnectedElasticsearchLogViewer.WrappedComponent.prototype.launchElasticQuery = jest.fn();
    const getAllDevices = ConnectedElasticsearchLogViewer.WrappedComponent.prototype.getAllDevices = jest.fn();
    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      inputs: myInput,
      tangoDB: 'testDB',
      messages: {},
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '42',
      devices: ["dserver/databaseds/2", "sys/tg_test/1"],
      elasticError: ""
    })

    const shallowElement = shallow(element);
    shallowElement.setProps({
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
    });

    expect(launchElasticQuery).toHaveBeenCalledTimes(1);
    expect(getAllDevices).toHaveBeenCalledTimes(1);

    shallowElement.find('#refresh-rate').simulate('change', {
      currentTarget: { "label": "5sec", "name": "5 seconds", "value": "5000" }
    });
    //check the changes
    expect(shallowElement.state('selectedRefresh')).toBe(5000);
    expect(launchElasticQuery).toHaveBeenCalledTimes(2);

    shallowElement.find('#filter-arrow').simulate('click');
    expect(shallowElement.html()).toContain('log-filter-collapsed');
    expect(shallowElement.html()).toContain('log-container-expanded');
    expect(shallowElement.state('showFilters')).toEqual(false);

    shallowElement.find('#filter-arrow').simulate('click');
    shallowElement.find('.paginator-button').at(0).simulate('click');
    expect(shallowElement.state('page')).toEqual(0);
    shallowElement.find('.paginator-button').at(2).simulate('click');

    shallowElement.find('.pause-button').childAt(0).simulate('click');
    expect(shallowElement.state('updating')).toEqual(false);
  })
});

describe("Test Elasticsearch log viewer with messages", () => {

  beforeEach(() => {
    // @ts-ignore
    delete window.location;
    // @ts-ignore
    window.location = new URL('https://k8s.stfc.skao.int/testdb/dashboard?id=64255c0fdd11d00018977284');
  });

  const messages = {
    '1': [
      {
        _index: 'filebeat-7.17.0-2023.08.22-022145',
        _id: 'L-xxHIoBlNxnKEU8j3j3',
        _score: null,
        _source: {
          container: {
            image: {
              name: 'registry.gitlab.com/ska-telescope/ska-tango-operator/ska-tango-operator:0.9.6'
            },
            runtime: 'containerd',
            id: 'a1f216b97faaea8cf50781257231354ad2456c1f302c8979a45a03163316a9ed'
          },
          agent: {
            hostname: 'rmdskadevdu014',
            name: 'rmdskadevdu014',
            id: '7ded50a7-c821-48e7-8bbf-c574af35dfd2',
            type: 'filebeat',
            ephemeral_id: 'e0a8407f-57d0-4fd2-86a4-11297fa70f7b',
            version: '7.17.0'
          },
          log: {
            file: {
              path: '/var/log/containers/ska-tango-operator0-controller-manager-fb65558d6-7k6x5_ska-tango-operator_manager-a1f216b97faaea8cf50781257231354ad2456c1f302c8979a45a03163316a9ed.log'
            },
            offset: 1551516
          },
          message: '1.0.1 | 2023-08-22 16:38:54,409.409Z | DEBUG | test.py#1 | This is a another info message',
          '@timestamp': '2023-08-22T08:50:51.486Z',
          ecs: {
            version: '1.12.0'
          },
          stream: 'stderr',
          host: {
            name: 'rmdskadevdu014'
          }
        },
        sort: [
          1692694251486
        ]
      },
      {
        _index: 'filebeat-7.17.0-2023.08.22-022145',
        _id: 'ZaRxHIoBeZ34t1fDjV2a',
        _score: null,
        _source: {
          container: {
            image: {
              name: 'registry.gitlab.com/ska-telescope/ska-tango-operator/ska-tango-operator:0.9.6'
            },
            runtime: 'containerd',
            id: 'a1f216b97faaea8cf50781257231354ad2456c1f302c8979a45a03163316a9ed'
          },
          kubernetes: {
            container: {
              name: 'manager'
            },
            node: {
              uid: '01ea5cbf-1248-4e6c-94a8-b024783bda81',
              hostname: 'rmdskadevdu014',
              name: 'rmdskadevdu014',
              labels: {
                'node-role_skatelescope_org/ci-worker': 'true',
                'kubernetes_io/hostname': 'rmdskadevdu014',
                'beta_kubernetes_io/os': 'linux',
                'kubernetes_io/arch': 'amd64',
                'kubernetes_io/os': 'linux',
                'beta_kubernetes_io/arch': 'amd64'
              }
            },
            pod: {
              uid: '6f37eb8b-845c-4538-9179-e69fcc57b66e',
              ip: '10.10.212.245',
              name: 'ska-tango-operator0-controller-manager-fb65558d6-7k6x5'
            },
            namespace: 'ska-tango-operator',
            namespace_uid: '3afec04a-3385-463d-81b5-a2c17eeed6ee',
            replicaset: {
              name: 'ska-tango-operator0-controller-manager-fb65558d6'
            },
            namespace_labels: {
              'kubernetes_io/metadata_name': 'ska-tango-operator',
              name: 'ska-tango-operator'
            },
            labels: {
              app: 'ska-tango-operator',
              'pod-template-hash': 'fb65558d6',
              'app_kubernetes_io/name': 'ska-tango-operator',
              'control-plane': 'controller-manager',
              'app_kubernetes_io/instance': 'ska-tango-operator0'
            },
            deployment: {
              name: 'ska-tango-operator0-controller-manager'
            }
          },
          log: {
            file: {
              path: '/var/log/containers/ska-tango-operator0-controller-manager-fb65558d6-7k6x5_ska-tango-operator_manager-a1f216b97faaea8cf50781257231354ad2456c1f302c8979a45a03163316a9ed.log'
            },
            offset: 1530810
          },
          message: '1.0.1 | 2023-08-22 16:38:54,409.409Z | INFO | test.py#1 | This is a info message',
          input: {
            type: 'container'
          },
          orchestrator: {
            cluster: {
              name: 'kubernetes',
              url: 'https://142.73.34.170:6443'
            }
          },
          '@timestamp': '2023-08-22T08:50:50.801Z',
          ecs: {
            version: '1.12.0'
          },
          stream: 'stderr',
          host: {
            name: 'rmdskadevdu014'
          }
        },
        sort: [
          1692694250801
        ]
      }
    ]
  }

  it("check refresh rate", async () => {

    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      inputs: myInput,
      tangoDB: 'testDB',
      messages: messages,
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '1',
      devices: ["dserver/databaseds/2", "sys/tg_test/1"],
      elasticError: ""
    })

    const shallowElement = shallow(element);
    shallowElement.setProps({
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
    });

    expect(shallowElement.html()).toContain('1.0.1  |  2023-08-22 16:38:54,409.409Z  |  DEBUG  |  test.py#1  |  This is a another info message');
  })

  it("Test textDisplay false", async () => {

    let element = React.createElement(ConnectedElasticsearchLogViewer.WrappedComponent, {
      mode: "run",
      inputs: {...myInput, textDisplay: false},
      tangoDB: 'testDB',
      messages: messages,
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
      widgets: widgets,
      variables: undefined,
      fetchElasticLogs: jest.fn(),
      id: '1',
      devices: ["dserver/databaseds/2", "sys/tg_test/1"],
      elasticError: ""
    })

    const shallowElement = shallow(element);
    shallowElement.setProps({
      selectedDashboard: selectedDashboard,
      dashboards: myDashboards,
    });

    expect(shallowElement.html()).not.toContain('1.0.1 | 2023-08-22 16:38:54,409.409Z | DEBUG | test.py#1 | This is a another info message');
    expect(shallowElement.html()).toContain('DEBUG');
  })
});

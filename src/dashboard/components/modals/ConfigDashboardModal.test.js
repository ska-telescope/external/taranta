import React from 'react'
import { configure, mount, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import ConfigDashboardModal from './ConfigDashboardModal'

configure({ adapter: new Adapter() });

const dashboardId = "6045fdf77a53fe001155cf8b";
let dashboardVariables = JSON.parse('[{ "id": "d5jeieka9dgg", "name": "var2", "class": "TangoTest", "device": "sys/tg_test/1" }, { "id": "kihdc2dgelha", "name": "var1", "class": "DataBase", "device": "sys/database/2" }]');
let tangoClass = JSON.parse(`[
    { "name": "DataBase", "devices": [
        { "name": "sys/database/2", "exported": true, "connected": true },
        { "name": "sys/database/1", "exported": true, "connected": true }
      ]
    },
    { "name": "DServer", "devices": [
        { "name": "dserver/DataBaseds/2", "exported": true, "connected": true },
        { "name": "dserver/Starter/35f2aa8f12d4", "exported": true, "connected": true },
        { "name": "dserver/Starter/f1a741b1aba7", "exported": true, "connected": false },
        { "name": "dserver/TangoAccessControl/1", "exported": true, "connected": true },
        { "name": "dserver/TangoTest/test", "exported": true, "connected": true },
        { "name": "dserver/tarantaTestDevice/test", "exported": true, "connected": false }
      ]
    },
    { "name": "Starter", "devices": [
        { "name": "tango/admin/35f2aa8f12d4", "exported": true, "connected": true },
        { "name": "tango/admin/f1a741b1aba7", "exported": true, "connected": false }
      ]
    },
    { "name": "TangoAccessControl", "devices": [
        { "name": "sys/access_control/1", "exported": true, "connected": true }
      ]
    },
    { "name": "TangoTest", "devices": [
        { "name": "sys/tg_test/1", "exported": true, "connected": true }
      ]
    },
    { "name": "tarantaTestDevice", "devices": [
        { "name": "test/tarantatestdevice/1", "exported": true, "connected": false }
      ]
    }
]`);

describe("Configure variable modal", () => {

    it('runs without crash with a empty variable list', () => {
        const element = React.createElement(ConfigDashboardModal, {
            id: dashboardId,
            name: "",
            environment: [],
            onClose: () => { },
            dashboardVariables: [],
            filterDashboardVariables: () => { },
            deleteDashboardVariable: () => { },
            addDashboardVariable: () => { },
            updateDashboardVariable: () => { },
            saveDashboardVariables: () => { },
            handleCancel: () => { },
            addUpdateEnvironment: () => { },
        })

        const shallowElement = mount(element);

        shallowElement.setState({
            showAddBox: false,
            editVariable: 0,
            confirmDelete: 0,
            addVariableName: '',
            addVariableClass: '',
            addVariableDevice: '',
            successMsg: '',
            showMsg: false,
            contentHeaderTitle: ['Variable Name', 'Class Name', 'Default Device', 'Actions']
        })

        expect(shallowElement.html()).toContain("No record found.");
    })

    it('runs show dashboard variable list', () => {
        const element = React.createElement(ConfigDashboardModal, {
            id: dashboardId,
            name: "",
            environment: [],
            onClose: () => { },
            dashboardVariables: dashboardVariables,
            filterDashboardVariables: () => { },
            deleteDashboardVariable: () => { },
            addDashboardVariable: () => { },
            updateDashboardVariable: () => { },
            saveDashboardVariables: () => { },
            handleCancel: () => { },
        })

        const shallowElement = mount(element);

        shallowElement.setState({
            showAddBox: false,
            editVariable: 0,
            confirmDelete: 0,
            addVariableName: '',
            addVariableClass: '',
            addVariableDevice: '',
            successMsg: '',
            showMsg: false,
            contentHeaderTitle: ['Variable Name', 'Class Name', 'Default Device', 'Actions']
        })

        expect(shallowElement.html()).toContain("TangoTest");
        expect(shallowElement.html()).not.toContain("No record found.");

    })

    it('edit dashboard variable', () => {
        const element = React.createElement(ConfigDashboardModal, {
            id: dashboardId,
            name: "",
            environment: [],
            onClose: () => { },
            dashboardVariables: dashboardVariables,
            filterDashboardVariables: () => { },
            deleteDashboardVariable: () => { },
            addDashboardVariable: () => { },
            updateDashboardVariable: () => { },
            saveDashboardVariables: () => { },
            handleCancel: () => { },
        })

        const shallowElement = shallow(element);

        shallowElement.setState({
            showAddBox: false,
            editVariable: "var2",
            confirmDelete: 0,
            addVariableName: '',
            addVariableClass: '',
            addVariableDevice: '',
            successMsg: '',
            showMsg: false,
            contentHeaderTitle: ['Variable Name', 'Class Name', 'Default Device', 'Actions'],
            updatedVariables: [],
            tangoClasses: tangoClass,
            tangoDevicesEdit: ['tango/admin/f1a741b1aba7,tango/admin/f1a741b1aba7']
        })

        shallowElement.find({ title: 'Edit Variable' }).simulate('click');
        expect(shallowElement.state('editVariable')).toBe("var1");

        //simulate change Class
        shallowElement.find('.list-class-name').simulate('change', { target: { value: 'Starter' } });

        //simulate change Device

        shallowElement.find('.list-device-name').simulate('change', { target: { value: 'tango/admin/f1a741b1aba7' } });
        //simulate click on Save
        shallowElement.find({ title: 'Save Variable' }).simulate('click');
        expect(shallowElement.state('editVariable')).toBe(0);

        //continue to edit the variable
        shallowElement.setState({ editVariable: "var2" });
        expect(shallowElement.state('editVariable')).toBe("var2");
        //test cancel button
        shallowElement.find({ title: 'Cancel' }).simulate('click');
        expect(shallowElement.state('editVariable')).toBe(0);

    })

    it('Add new variable', () => {
        const element = React.createElement(ConfigDashboardModal, {
            id: dashboardId,
            name: "",
            environment: [],
            onClose: () => { },
            dashboardVariables: dashboardVariables,
            filterDashboardVariables: () => { },
            deleteDashboardVariable: () => { },
            addDashboardVariable: () => { },
            updateDashboardVariable: () => { },
            saveDashboardVariables: () => { },
            handleCancel: () => { },
        })

        const shallowElement = mount(element);

        shallowElement.setState({
            showAddBox: false,
            editVariable: 0,
            confirmDelete: 0,
            addVariableName: '',
            addVariableClass: '',
            addVariableDevice: '',
            successMsg: '',
            showMsg: false,
            contentHeaderTitle: ['Variable Name', 'Class Name', 'Default Device', 'Actions'],
            updatedVariables: [],
            tangoClasses: tangoClass
        })

        expect(shallowElement.state('showAddBox')).toBe(false)
        shallowElement.find({ title: 'Add New Dashboard Variable' }).simulate('click');

        //open add new variable pane
        expect(shallowElement.state('showAddBox')).toBe(true)

        //create variable
        shallowElement.find({ id: 'add-new-var-name' }).simulate('change', { target: { value: 'varTest', name: "add-new-var-name" } });

        //define a name
        expect(shallowElement.state('addVariableName')).toBe('varTest');

        //change the class
        shallowElement.find({ id: 'add-new-var-class' }).simulate('change', { target: { value: 'DServer', name: "add-var-class" } });
        expect(shallowElement.state('addVariableClass')).toBe('DServer');

        //change the default device
        shallowElement.find({ id: 'add-new-var-device' }).simulate('change', { target: { value: 'dserver/Starter/35f2aa8f12d4', name: "add-var-device" } });
        expect(shallowElement.state('addVariableDevice')).toBe('dserver/Starter/35f2aa8f12d4');

        //change the class
        shallowElement.find({ id: 'add-new-var-class' }).simulate('change', { target: { value: 'WebjiveTestDevice', name: "add-var-class" } });
        expect(shallowElement.state('addVariableClass')).toBe('WebjiveTestDevice');
        shallowElement.find({ id: 'add-new-var-device' }).simulate('change', { target: { value: 'test/webjivetestdevice/1', name: "add-var-device" } });
        expect(shallowElement.state('addVariableDevice')).toBe('test/webjivetestdevice/1');

        //save variable
        shallowElement.find({ type: 'submit' }).simulate('submit');

        expect(shallowElement.state('showAddBox')).toBe(false);
        expect(shallowElement.state('showMsg')).toBe(true);
        expect(shallowElement.html()).toContain('Variable added successfully');
    })

    it('environment happy path', () => {
        const validateEnv = jest.fn();
        const handleEnvTags = jest.fn();
        const saveEnvironment = jest.fn();
        const addUpdateEnvironment = jest.fn();

        const element = React.createElement(ConfigDashboardModal, {
            id: dashboardId,
            name: "My Dash",
            environment: ["http://localhost:3000/testdb/dashboard"],
            onClose: () => { },
            dashboardVariables: dashboardVariables,
            filterDashboardVariables: () => { },
            deleteDashboardVariable: () => { },
            addDashboardVariable: () => { },
            updateDashboardVariable: () => { },
            saveDashboardVariables: () => { },
            handleCancel: () => { },
            validateEnv: () => validateEnv,
            handleEnvTags: () => handleEnvTags,
            saveEnvironment: () => saveEnvironment,
            addUpdateEnvironment: () => addUpdateEnvironment
        })

        const wrapper = mount(element);

        wrapper.setState({
            showAddBox: false,
            editVariable: 0,
            confirmDelete: 0,
            addVariableName: '',
            addVariableClass: '',
            addVariableDevice: '',
            successMsg: '',
            showMsg: false,
            contentHeaderTitle: ['Variable Name', 'Class Name', 'Default Device', 'Actions'],
            updatedVariables: [],
            tangoClasses: tangoClass
        })

        expect(wrapper.html()).toContain("http://localhost:3000/testdb/dashboard");

        wrapper.find("a.react-tagsinput-remove").simulate("click");
        expect(wrapper.html()).not.toContain("http://localhost:3000/testdb/dashboard");
        expect(wrapper.state('environment')).toHaveLength(0);

        wrapper.find({ id: 'save-env' }).at(1).simulate('click');

        wrapper.find('.react-tagsinput-input').simulate('change', { target: { value: 'https://k8s.stfc.skao.int/taranta-namespace/taranta' } });
        expect(wrapper.html()).toContain('https://k8s.stfc.skao.int/taranta-namespace/taranta');
        wrapper.find('.react-tagsinput-input').simulate('keypress', { key: 'Enter' });
        wrapper.find('.react-tagsinput-input').simulate('keydown', { keyCode: 13 });
    })
})